\documentclass{beamer}

\title{A Quick-Start Guide to Using Vi(m) Effectively}
\author{Jamie Tanna, \url{https://jvt.me}}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% Material Theme {{{
\usetheme{material}

\useLightTheme
\usePrimaryPurple
\useAccentLightBlue
% }}}

\usepackage{hyperref}
\hypersetup{%
	colorlinks,
	linkcolor=accent,
	urlcolor=accent
}

% Allow us to use colours a bit more powerfully
%\usepackage[dvipsnames]{xcolor}
%% Allow us to link to document references, URLs, and more
%\usepackage[hidelinks]{hyperref}

% Set up our links to be specific colours
%\hypersetup{%
%	colorlinks = true,
%    linkcolor={red!50!black},	% maroon
%    citecolor={blue!50!black},	% dark blue
%    urlcolor={blue!80!black}	% dark blue
%}

% It's often useful to define terms in the footnote; this command makes it much
% less verbose in the source code
\newcommand*{\footnotedef}[2]{#1\footnote{\textbf{#1}: #2}}

% Decrease the default margins, so we don't have a ridiculous amount of blank space
% \usepackage[margin=2.3cm, top=1.8cm, bottom=1.8cm]{geometry}

% We will almost always want to be using an A4-sized page
% \geometry{a4paper}

\newcommand*{\red}[1]{{\color{red}#1}}
\newcommand*{\TODO}{{\color{red}TODO}}

\usepackage{nameref}
\makeatletter
% via https://tex.stackexchange.com/a/183357
\newcommand*{\getSectionTitle}{\let\hyperlink\@secondoftwo\insertsection}
\newcommand*{\getSubSectionTitle}{\let\hyperlink\@secondoftwo\insertsubsection}
\newcommand*{\currentname}{\getSubSectionTitle}
\makeatother

\setcounter{tocdepth}{1}

\usepackage{listings}
\lstset{
   breaklines=true,
   basicstyle=\ttfamily\tiny}

\setbeamerfont*{sectiontitle}{size=\LARGE}
\AtBeginSection[]%
{%
	\makebox[\linewidth][c]{%
		\begin{minipage}[t][\paperheight]{\paperwidth}
			\raggedright
			\begin{tcolorbox}[colback=primary, enhanced, sharpish corners=all, boxrule=0mm, coltext=textPrimary,
					fuzzy shadow={0mm}{-0.6mm}{0mm}{0.2mm}{shadow!40!BGgrey03}, % bottomSmall
					fuzzy shadow={0mm}{-0.2mm}{0mm}{0.2mm}{shadow!20!BGgrey03}, % bottomBig
					fuzzy shadow={0mm}{ 0.6mm}{0mm}{0.2mm}{shadow!40!primary}, % topSmall
					fuzzy shadow={0mm}{ 0.2mm}{0mm}{0.2mm}{shadow!20!primary}, % topBig
				width=\paperwidth, height=0.6\paperheight, flushright upper, valign=bottom, boxsep=0.5cm]
				{\usebeamerfont{sectiontitle} \getSectionTitle}\\
			\end{tcolorbox}

			%\begin{tcolorbox}[arc=5mm,width=10mm,height=10mm, enhanced, %
			%colback=accent, coltext=textAccent, %
			%fuzzy shadow={0mm}{ 0.9mm}{ 0.6mm}{0.2mm}{shadow!20!primary}, % top
			%fuzzy shadow={0mm}{-0.6mm}{-0.1mm}{0.2mm}{shadow!40!BGgrey03}, % bottomSmall
			%fuzzy shadow={0mm}{-0.2mm}{-0.2mm}{0.2mm}{shadow!20!BGgrey03}, % bottomBig
			%left=1.5mm, right=1.5mm, top=1.5mm, bottom=1.5mm, boxsep=0mm, %
			%boxrule=0mm, enlarge left by=10mm, enlarge top by=-10mm]%
			%\includegraphics[width=7mm]{\iconFolder/ic_account_circle_48px}
			%\end{tcolorbox}
		\end{minipage}%
	}%
	\vfill
}

\begin{document}
\maketitle

\begin{frame}[allowframebreaks]{Outline}
	\tableofcontents
\end{frame}

% sec:preface {{{
\section{Preface}
\label{sec:preface}

% sub:the_structure_of_this_talk {{{
\subsection{The Structure of this Talk}
\label{sub:the_structure_of_this_talk}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item This talk doesn't expect any Vi(m) knowledge.
		\item Feel free to ask questions as and when you want!
		\item This talk is expected to be given in an interactive form, swapping between slides and editor regularly.
		\item This talk is released as Free Software under the GNU General Public License version 3, and can be found at \url{https://gitlab.com/jamietanna/talks}.
	\end{itemize}
\end{frame}
% }}}

% sub:how_i_use_vim {{{
\subsection{How I use Vim}
\label{sub:how_i_use_vim}

\begin{frame}[t]{\currentname}
	I'm a very heavy Vim user - prioritising Vim for all my development where possible. I have my \href{https://gitlab.com/jamietanna/dotfiles-arch}{dotfiles available on GitLab}.

	~\\
	I started off by originally using Sublime Text as my main editor, but one day I was watching the brilliant \href{}{\textit{Destroy All Software}} talks, and was absolutely amazed and inspired at the speed Gary was navigating source files at. It was at this point I wanted to learn an editor very well\footnote{As well as following the \textit{Pragmatic Programmer}'s advice of ``Use a Single Editor Well''}.

	~\\
	However, I had heard that Vim had a steep learning curve, and wasn't quite ready for that level of involvement. So instead, I looked into Emacs (controversial, I know!) and for a good couple of months I used it as my daily driver.

\end{frame}
\begin{frame}[t]{\currentname}
	But there was a big issue - SSHing into servers and working on machines I didn't own/use regularly meant that I had to pull around 160MB of dependencies to get basic text editing. That wasn't ideal!

	~\\
	Therefore I looked into using Vim, and haven't really looked back. I've gotten my configs to a level where I can be productive, and am still constantly learning, a couple of years on.
\end{frame}
% }}}

% }}}

% sec:what_is_vi_m_ {{{
\section{What is Vi(m)?}
\label{sec:what_is_vi_m_}

% sub:vi {{{
\subsection{Vi}
\label{sub:vi}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item First released in 1976, by Bill Joy, as a \texttt{vi}sual editor for the line editor \texttt{ex}
		\item Lots of different versions around, but standardised in the Single UNIX Spec and POSIX.
	\end{itemize}
\end{frame}

% }}}

% sub:vim {{{
\subsection{Vim}
\label{sub:vim}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item Created by Bram Moolenaar (BDFL), first public release in 1991
		\item \texttt{VI iMproved}
		\item Both CLI and GUI versions
	\end{itemize}
\end{frame}
% }}}

% }}}

% sec:why_should_i_use_it_ {{{
\section{Why Should I Use It?}
\label{sec:why_should_i_use_it_}

% sub:modal_editing {{{
\subsection{Modal Editing}
\label{sub:modal_editing}

\begin{frame}[t]{\currentname}
	Modal editing is the ability to switch between different modes:

	\begin{itemize}
		\item \texttt{insert} mode -- allow entering of text (\texttt{i} at cursor, \texttt{a} after cursor)
		\item \texttt{normal} mode -- used for navigation and manipulation (default, \texttt{ESC})
		\item \texttt{command} mode -- usually denoted by a colon then the command, i.e. \texttt{:quit}
	\end{itemize}

	Now, this doesn't necessarily sound better, but when you get used to it, it makes a lot more sense, especially as it means that you can keep using your keyboard for many different things.
\end{frame}

% }}}

% sub:reduction_of_keystrokes {{{
\subsection{Reduction of Keystrokes}
\label{sub:reduction_of_keystrokes}

\begin{frame}[t]{\currentname}
	As a person who constantly wants to improve my workflow, and reduce the number of tasks I have to complete a given project, Vim is great for me. I'm constantly learning new ways to optimise the number of keystrokes that are required to perform given steps.
\end{frame}
% }}}

% sub:availability {{{
\subsection{Availability}
\label{sub:availability}

\begin{frame}[t]{\currentname}
	As mentioned, Vi is going to be available on any POSIX system you can touch -- so if you are ever going to be working on a POSIX machine, you can rest assured you'll be able to run Vi.

	~\\
	This means that you should be prepared to make simple edits for when you need to, as well as potentially even more involved changes and development work.
\end{frame}

% }}}

% }}}

% sec:how_do_i_perform_basic_tasks_ {{{
\section{How Do I Perform Basic Tasks?}
\label{sec:how_do_i_perform_basic_tasks_}

% sub:movement {{{
\subsection{Movement}
\label{sub:movement}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item \texttt{h} -- $\leftarrow$
		\item \texttt{j} -- $\downarrow$
		\item \texttt{k} -- $\uparrow$
		\item \texttt{l} -- $\rightarrow$
		\item Why not just use arrow keys?
			\begin{itemize}
				\item ``It's not the Vim way''
				\item Movement away from the ``home row''\footnote{i.e. the middle row, starting \texttt{asdf}} is the exact opposite of what you want to be doing in Vim
			\end{itemize}
	\end{itemize}
\end{frame}

% }}}

% sub:searching_for_content {{{
\subsection{Searching for Content}
\label{sub:searching_for_content}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item In order to search for a given string, you can use \texttt{/term} from \texttt{normal} mode
		\item To search backwards, you can use \texttt{?term} from \texttt{normal} mode
		\item Once searching, you may want to use \texttt{n} to go to the next result and \texttt{N} to go to the previous result
	\end{itemize}
\end{frame}

% }}}

% sub:changing_text {{{
\subsection{Changing Text}
\label{sub:changing_text}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item \texttt{c}hange  \texttt{<motion>}
		\item \texttt{d}elete  \texttt{<motion>}
		\item \texttt{r}eplace \texttt{<motion>}
	\end{itemize}
\end{frame}


% }}}

% sub:copy_paste {{{
\subsection{Copy + Paste}
\label{sub:copy_paste}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item \texttt{y}ank
		\item \texttt{p}aste after the cursor
		\item \texttt{P}aste before the cursor
	\end{itemize}
\end{frame}

% }}}

% sub:undo_redo {{{
\subsection{Undo/Redo}
\label{sub:undo_redo}

\begin{frame}[t]{\currentname}
	Sometimes you'll make changes you didn't mean to. You can use \texttt{u} to undo the change, and \texttt{C-r} to redo it.

\end{frame}
% }}}

% sub:quitting_saving_files {{{
\subsection{Quitting/Saving Files}
\label{sub:quitting_saving_files}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item \texttt{:q(uit)} - but stop us if we have unsaved content
		\item \texttt{:w(rite)}
		\item \texttt{:wq} -- combination of write and quit

		\item \texttt{:q(uit)!} -- quit, don't care about any changes we have
		\item \texttt{:w(rite)!} -- write, useful to overwrite a file if it's set as readonly
	\end{itemize}
\end{frame}

% }}}

% }}}

% sec:wait_why_is_everything_done_in_a_really_weird_way_ {{{
\section{Wait, Why is Everything Done in a Really Weird Way?}
\label{sec:wait_why_is_everything_done_in_a_really_weird_way_}

% sub:its_a_grammar {{{
\subsection{It's a Grammar and Vocabulary}
\label{sub:its_a_grammar}

\begin{frame}[t]{\currentname}
	Vim's actions and keys are like a language -- for instance, you can \texttt{c}hange \texttt{a} \texttt{w}ord (which is done by the keypresses \texttt{caw}) or \texttt{d}elete \texttt{i}nside the \texttt{(}...).
\end{frame}

% }}}

% sub:operators {{{
\subsection{Operators}
\label{sub:operators}

\begin{frame}[fragile]{\currentname}

	For instance, there are many actions that can occur within this vocabulary, such as:

	\begin{tabular}{c c}
		\textbf{Operator} & \textbf{Meaning} \\\hline
		\texttt{c} & Change \\
		\texttt{d} & Delete \\
		\texttt{r} & Replace \\
		\texttt{y} & Yank \\
		\texttt{\textasciitilde} & Swap Case \\
		\texttt{gq} & Perform Text Formatting \\
	\end{tabular}

\end{frame}
% }}}

% sub:motions {{{
\subsection{Motions}
\label{sub:motions}

\begin{frame}[t]{\currentname}
	\begin{tabular}{c c}
		\textbf{Motion} & \textbf{Meaning} \\\hline
		\texttt{h} & move $\leftarrow$\footnote{Note that these movements are inclusive; this will i.e. delete the character under the cursor, and the character to the left}\\
		\texttt{j}	& move $\downarrow$ \\
		\texttt{k}	& move $\uparrow$   \\
		\texttt{l}	& move $\rightarrow$\\
		\texttt{w}	& move to the next word\\
		\texttt{b}	& move to the beginning of the current word\\
		\texttt{e}	& move to the end of the current word\\
		\texttt{4j}	& four lines down\\
	\end{tabular}
\end{frame}

\begin{frame}[t]{\currentname}
	Additionally, some Vim-only text objects:
% \footnote{A \textbf{WORD} consists of a sequence of non-blank characters, separated with white space.  An empty line is also considered to be a WORD.}
	\begin{tabular}{c c}
		\textbf{Motion} & \textbf{Meaning} \\\hline
		\texttt{aw}	& ``a word''\\
		\texttt{iw}	& ``inside word'' \\
		\texttt{as}	& ``a sentence'' \\
		\texttt{ap}	& ``a paragraph'' \\
	\end{tabular}

\end{frame}

% }}}

% sub:putting_it_together {{{
\subsection{Putting it Together}
\label{sub:putting_it_together}

\begin{frame}[t]{\currentname}

	\begin{tabular}{c c}
		\textbf{Keypress} & \textbf{Action} \\\hline
		\texttt{caw}	& change ``a word''\\
		\texttt{diw}	& delete ``inside a word''\\
		\texttt{yas}	& yank/copy ``a sentence'' \\
		\texttt{bw}		& backwards a word\\
	\end{tabular}
\end{frame}

% }}}


% sub:conventions {{{
\subsection{Conventions}
\label{sub:conventions}

\begin{frame}[t]{\currentname}

	Some conventions:
	\begin{itemize}
		\item Repeated actions, such as deleting a line, can be done with \texttt{dd}.
		\item Capitalisation often denotes ``until the end of line''; Deleting to the end of a line can be done with \texttt{D} (or normally as \texttt{d\$})
	\end{itemize}
\end{frame}

% }}}


% }}}

% sub:composability {{{
\subsection{Composability}
\label{sub:composability}

\begin{frame}[t]{\currentname}
	As mentioned above, it's all about having a language you can use. The composability comes when you can do things like:
	\begin{itemize}
		\item \texttt{dap}
		\item \texttt{ci"}
		\item \texttt{ct>}
		\item \texttt{=\%}
		\item \texttt{5dw}
	\end{itemize}
\end{frame}

% }}}

% }}}

% sec:tips_and_tricks {{{
\section{Tips and Tricks}
\label{sec:tips_and_tricks}

% sub:write_to_a_file_as_root {{{
\subsection{Write to a File As Root}
\label{sub:write_to_a_file_as_root}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item Have you ever forgotten to \texttt{sudo vim ...}?
		\item You can avoid this by running \texttt{:w !sudo tee \% >/dev/null}
			\begin{itemize}
				\item \texttt{:w}rite
				\item \texttt{\!} into a process call
				\item \texttt{sudo tee} run the \texttt{tee} command as root
				\item \texttt{\%} with the current filename as an argument
				\item \texttt{>/dev/null} redirect output (via \texttt{tee}) away, so we don't have to see it
			\end{itemize}
	\end{itemize}
\end{frame}
% }}}

% sub:word_wrapping {{{
\subsection{Word Wrapping}
\label{sub:word_wrapping}

\begin{frame}[fragile]{\currentname}
	If you wish to have your text formatted within the right character limit, for instance, in a commit message, you can use \texttt{gq}.

	It converts:
	\begin{lstlisting}
Make sure that we rewrite any Git URLs such that we're always going to be able to `git push` via SSH. This means that we can clone over HTTPS, but push through SSH.
	\end{lstlisting}
	To:

	\begin{lstlisting}
Make sure that we rewrite any Git URLs such that we're always going to
be able to `git push` via SSH. This means that we can clone over HTTPS,
but push through SSH.
	\end{lstlisting}

\end{frame}
% }}}

% }}}

% sec:how_can_i_best_get_used_to_it_ {{{
\section{How Can I Best Get Used To It?}
\label{sec:how_can_i_best_get_used_to_it_}

% sub:phasing_it_in {{{
\subsection{Phasing it in}
\label{sub:phasing_it_in}

\begin{frame}[t]{\currentname}
	In order to get some exposure to something like Vi(m), it's easiest by actually using it. However, by switching to solely Vim, you'll hurt your own productivity.

	The best way to get going is to start slowly introducing it into your workflow with little bits here and there.
\end{frame}

% }}}

% sub:use_it_for_writing_commit_messages {{{
\subsection{Use it for Writing Commit Messages}
\label{sub:use_it_for_writing_commit_messages}

\begin{frame}[t]{\currentname}
	By default, your commit message is through Vi(m). Therefore, instead of running \texttt{git commit -m "..."}, just run \texttt{git commit}.

	This will get you using it for basic text editing, and will start you off in a nice way, without having to take the plunge to learning it for your main editor.
\end{frame}
% }}}

% sub:vimtutor {{{
\subsection{\texttt{vimtutor}}
\label{sub:vimtutor}

\begin{frame}[t]{\currentname}
	\texttt{vimtutor} is a great tutorial that runs you through all the basics for Vim, such as those listed here, in a much more interactive way.
\end{frame}

% }}}

% sub:learn_x_in_y_minutes {{{
\subsection{Learn X In Y Minutes}
\label{sub:learn_x_in_y_minutes}

\begin{frame}[t]{\currentname}
	A great resource that can be used as a quick reference/cheat sheet - \url{https://learnxinyminutes.com/docs/vim/}
\end{frame}

% }}}
% }}}

% sub:installing_in_your_preferred_editor {{{
\subsection{Installing in your Preferred Editor}
\label{sub:installing_in_your_preferred_editor}

\begin{frame}[t]{\currentname}

	As mentioned, there are plugins that can be installed in your existing editor - some of which can be found at:

	\begin{itemize}
		\item \href{https://github.com/JetBrains/ideavim}{IntelliJ IdeaVim}
		\item \href{https://atom.io/packages/vim-mode}{Atom vim-mode}
		\item \href{https://www.emacswiki.org/emacs/Evil}{Emacs Evil mode}
	\end{itemize}
\end{frame}

% }}}

% }}}


\end{document}
