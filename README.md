# Talks

A repository to collect various talks, presentations and workshops I have done over time.

I felt that instead of having multiple repos, with one per talk, I would instead organise them into a single repo, to make it easier to share and publicise.
