\documentclass{beamer}

\title{Kickstarting your Automated Build and Continuous Delivery platform with GitLab CI}
\author{Jamie Tanna, \url{https://jvt.me}}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% Material Theme {{{
\usetheme{material}

\useLightTheme
\usePrimaryDeepOrange
\useAccentAmber
% }}}

\usepackage{hyperref}
\hypersetup{%
	colorlinks,
	linkcolor=accent,
	urlcolor=accent
}

% Allow us to use colours a bit more powerfully
%\usepackage[dvipsnames]{xcolor}
%% Allow us to link to document references, URLs, and more
%\usepackage[hidelinks]{hyperref}

% Set up our links to be specific colours
%\hypersetup{%
%	colorlinks = true,
%    linkcolor={red!50!black},	% maroon
%    citecolor={blue!50!black},	% dark blue
%    urlcolor={blue!80!black}	% dark blue
%}

% It's often useful to define terms in the footnote; this command makes it much
% less verbose in the source code
\newcommand*{\footnotedef}[2]{#1\footnote{\textbf{#1}: #2}}

% Decrease the default margins, so we don't have a ridiculous amount of blank space
% \usepackage[margin=2.3cm, top=1.8cm, bottom=1.8cm]{geometry}

% We will almost always want to be using an A4-sized page
% \geometry{a4paper}

\newcommand*{\red}[1]{{\color{red}#1}}
\newcommand*{\TODO}{{\color{red}TODO}}

\usepackage{nameref}
\makeatletter
% via https://tex.stackexchange.com/a/183357
\newcommand*{\getSectionTitle}{\let\hyperlink\@secondoftwo\insertsection}
\newcommand*{\getSubSectionTitle}{\let\hyperlink\@secondoftwo\insertsubsection}
\newcommand*{\currentname}{\getSubSectionTitle}
\makeatother

\setcounter{tocdepth}{1}

% Syntax highlighting {{{
\usepackage{listings}
\lstset{
   breaklines=true,
   basicstyle=\ttfamily\tiny}
\usepackage{minted}
% }}}

\setbeamerfont*{sectiontitle}{size=\LARGE}
\AtBeginSection[]%
{%
	\makebox[\linewidth][c]{%
		\begin{minipage}[t][\paperheight]{\paperwidth}
			\raggedright
			\begin{tcolorbox}[colback=primary, enhanced, sharpish corners=all, boxrule=0mm, coltext=textPrimary,
					fuzzy shadow={0mm}{-0.6mm}{0mm}{0.2mm}{shadow!40!BGgrey03}, % bottomSmall
					fuzzy shadow={0mm}{-0.2mm}{0mm}{0.2mm}{shadow!20!BGgrey03}, % bottomBig
					fuzzy shadow={0mm}{ 0.6mm}{0mm}{0.2mm}{shadow!40!primary}, % topSmall
					fuzzy shadow={0mm}{ 0.2mm}{0mm}{0.2mm}{shadow!20!primary}, % topBig
				width=\paperwidth, height=0.6\paperheight, flushright upper, valign=bottom, boxsep=0.5cm]
				{\usebeamerfont{sectiontitle} \getSectionTitle}\\
			\end{tcolorbox}

			%\begin{tcolorbox}[arc=5mm,width=10mm,height=10mm, enhanced, %
			%colback=accent, coltext=textAccent, %
			%fuzzy shadow={0mm}{ 0.9mm}{ 0.6mm}{0.2mm}{shadow!20!primary}, % top
			%fuzzy shadow={0mm}{-0.6mm}{-0.1mm}{0.2mm}{shadow!40!BGgrey03}, % bottomSmall
			%fuzzy shadow={0mm}{-0.2mm}{-0.2mm}{0.2mm}{shadow!20!BGgrey03}, % bottomBig
			%left=1.5mm, right=1.5mm, top=1.5mm, bottom=1.5mm, boxsep=0mm, %
			%boxrule=0mm, enlarge left by=10mm, enlarge top by=-10mm]%
			%\includegraphics[width=7mm]{\iconFolder/ic_account_circle_48px}
			%\end{tcolorbox}
		\end{minipage}%
	}%
	\vfill
}

\begin{document}
\maketitle

\begin{frame}[allowframebreaks]{Outline}
	\tableofcontents
\end{frame}

% sec:preface {{{
\section{Preface}
\label{sec:preface}

% sub:the_structure_of_this_talk {{{
\subsection{The Structure of this Talk}
\label{sub:the_structure_of_this_talk}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item This talk doesn't expect any GitLab knowledge as well as limited PHP knowledge.
		\item Feel free to ask questions as and when you want!
		\item This talk is expected to be given in an interactive form, showing code snippets, and sharing how GitLab works with them.
		\item This talk is released as Free Software under the GNU General Public License version 3, and can be found at \url{https://gitlab.com/jamietanna/talks}, expanded details of which can be read at \url{https://jvt.me/posts/2017/03/25/why-you-should-use-gitlab/}
	\end{itemize}
\end{frame}
% }}}

% sub:what_s_this_continuous_integration_thing_ {{{
\subsection{What's this Continuous Integration Thing?}
\label{sub:what_s_this_continuous_integration_thing_}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item ``Continuous Integration (CI) is a development practice that requires developers to integrate code into a shared repository several times a day.''\cite{TWCI}
		\item Usually used to just mean ``automated build system''
		\item All about ensuring that long-term feature branches aren't diverged from \texttt{master} or \texttt{develop}
	\end{itemize}
\end{frame}
% }}}

% }}}

% sec:why_gwhy_gitlab_tlab_ {{{
\section{Why GitLab?}
\label{sec:why_gitlab_}

\begin{frame}[t]{}
	\vspace*{\fill}
	\begin{card}
		``I would say the main difference is GitHub is designed for people who want to collaborate on writing software, but that’s where it stops. GitLab is designed for people to collaborate and take that software right through to build and deployment.'' -- eddieajua\cite{DearGitHubGitLab}
	\end{card}
	\vspace*{\fill}
\end{frame}

% sub:unlimited_most_of_the_thunlimited_most_of_the_things_ngs_ {{{
\subsection{Unlimited (Most) Of the Things!}
\label{sub:unlimited_most_of_the_things_}

\begin{frame}[t]{\currentname}
\begin{itemize}
	\item Until early April, everything was unlimited and free
	\item Extra features like LDAP sync and greater control over process is done through GitLab EES and EEP
	\item If you're using more than ``2000 minutes a month ... private projects''\footnote{a pipeline of 10 minutes, five times a day, every working day} in a \textit{group} based on \textit{shared runner}, you'll have to pay a bit more.\cite{GitLabPaidSub}
\end{itemize}
\end{frame}
% }}}

% sub:open_source {{{
\subsection{Open Source}
\label{sub:open_source}

\begin{frame}[t]{\currentname}
\begin{itemize}
	\item GitLab is \textit{actually} Open Source, MIT Expat
	\item GitLab CE is available and free to use as you wish
\end{itemize}
\end{frame}
% }}}

% sub:ci {{{
\subsection{CI}
\label{sub:ci}

\begin{frame}[t]{\currentname}
	\begin{itemize}
	\item Docker-based pipelines
		\begin{itemize}
			\item Ability to pull in services as well as build images as part of it
		\end{itemize}

	\item Registry - private registry
		\begin{itemize}
			\item Multiple images per project (9.1 CE+)
		\end{itemize}

	\item Environments
		\begin{itemize}
			\item Track what's where
			\item \texttt{git checkout environments/production/123}
		\end{itemize}
	\item Review Apps
	\begin{itemize}
		\item Ever wanted to see what UI changes a MR has made, without needing to download and run the code?
	\end{itemize}
\end{itemize}
\end{frame}

% }}}

% sub:one_stop_shop {{{
\subsection{One Stop Shop}
\label{sub:one_stop_shop}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item GitLab as ``the platform'' rather than just your code
	\item ``Why can't you run applications \textit{on} GitLab as well?''
	\end{itemize}

	\includegraphics[width=\textwidth]{images/gitlab-platform}
\end{frame}
% }}}

% sub:open_communications_and_operations {{{
\subsection{Open Communications and Operations}
\label{sub:open_communications_and_operations}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item \texttt{rm -rf /var/database} - \url{https://about.gitlab.com/2017/02/10/postmortem-of-database-outage-of-january-31/}
		\item May 8th Redis outage - \url{https://gitlab.com/gitlab-com/infrastructure/issues/1762}
		\item Planned changes - \url{https://gitlab.com/gitlab-com/infrastructure/issues/1747}
		\item Open operations! \url{https://gitlab.com/gitlab-com/runbooks}
\end{itemize}
\end{frame}

% }}}

% sub:caveats {{{
\subsection{Caveats}
\label{sub:caveats}

\begin{frame}[t]{\currentname}
	\begin{itemize}
		\item Open Core Model
			\begin{itemize}
				\item Income is sourced from EE, providing time cost to community work
				\item Community \href{https://gitlab.com/gitlab-org/gitlab-ce/issues/14605}{asks} for something from EE? They \href{https://gitlab.com/gitlab-org/gitlab-ce/issues/14605}{can get it}!
			\end{itemize}

		\item Pipelines can't be used for arbitrary tasks as easily as i.e. Jenkins
		\item Infrastructure stability
		\item Lack of community (where this session comes in)
	\end{itemize}

\end{frame}
% }}}

% }}}

% sec:adding_gitlab_integration_to_an_existing_project {{{
\section{Adding GitLab Integration to an Existing Project}
\label{sec:adding_gitlab_integration_to_an_existing_project}

% sub:our_sample_project {{{
\subsection{Our Sample Project}
\label{sub:our_sample_project}

\begin{frame}[t]{\currentname}
	The project can be found at \url{https://gitlab.com/jamietanna/slim}, and is an import of the SLiM PHP framework.
\end{frame}
% }}}

% sub:surveying_the_.travis.yml_file {{{
\subsection{Surveying the \texttt{.travis.yml} file}
\label{sub:surveying_the_.travis.yml_file}

\begin{frame}[fragile]{\currentname}
As we're currently set up using an existing CI platform, it's fairly easy to see how we want to configure GitLab's CI:

% TODO: Not small enough!
\inputminted[breaklines,fontsize=\tiny,firstline=3,lastline=22]{yaml}{src/slim-travis.yml}

\end{frame}

% }}}

% sub:creating_our_first_test_case {{{
\subsection{Creating Our First Test Case}
\label{sub:creating_our_first_test_case}

\begin{frame}[fragile]{\currentname}
	Let's start by testing the first PHP version that has a corresponding Docker image:

    \begin{minted}[breaklines,fontsize=\scriptsize]{yaml}
    before_script:
      - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
      - php composer-setup.php --install-dir=/usr/bin
      - ln -s /usr/bin/composer.phar /usr/bin/composer

    test:unit:
      image: php:5.6-alpine
      script:
        - composer require satooshi/php-coveralls:1.* squizlabs/php_codesniffer:2.* -n
        - vendor/bin/phpunit --coverage-clover clover.xml
        - vendor/bin/phpcs
    \end{minted}
\end{frame}

% }}}

% sub:expanding_to_different_php_versions {{{
\subsection{Expanding to Different PHP Versions}
\label{sub:expanding_to_different_php_versions}

\begin{frame}[fragile]{\currentname}
	Now we should try and test against different PHP versions, to ensure that we cover our bases:

    \begin{minted}[breaklines,fontsize=\scriptsize]{yaml}
    test:unit:
    test:php56:
      image: php:5.6-alpine
      script:
        - composer require satooshi/php-coveralls:1.* squizlabs/php_codesniffer:2.* -n
        - vendor/bin/phpunit --coverage-clover clover.xml
        - vendor/bin/phpcs

    test:php70:
      image: php:7.0-alpine
      script:
        - composer install
        - vendor/bin/phpunit

    test:php71:
      image: php:7.1-alpine
      script:
        - composer install
        - vendor/bin/phpunit
    \end{minted}

\end{frame}
% }}}

% sub:is_this_safe_ {{{
\subsection{Is This Safe?}
\label{sub:is_this_safe_}

\begin{frame}[fragile]{\currentname}
Can I actually deploy this application? Is it secure enough?

Let's first look at checking for dependencies:

\begin{minted}[breaklines,fontsize=\scriptsize]{yaml}
 stages:
  - lint
  - test
 - security

...

security:dependency:
  image: php:alpine
  stage: security
  script:
    - php -r "copy('http://get.sensiolabs.org/security-checker.phar', 'security-checker.phar');"
    - composer install
    - php security-checker.phar security:check composer.lock
\end{minted}
\end{frame}

% }}}

% sub:hey_why_are_you_using_three_tabs_ {{{
\subsection{Hey, Why Are You Using Three Tabs?}
\label{sub:hey_why_are_you_using_three_tabs_}

\begin{frame}[t]{\currentname}
	Ever worked with someone so infuriating, it felt like they had a different standard on what code should look like?\\

	\uncover<2->{ \includegraphics[width=\textwidth]{images/hot-dog-spacing} }

	\uncover<3->{ Code style should be enforced at the CI and testing level, to ensure that no one can complain on a merge request that there's the wrong setup used. }
\end{frame}

\begin{frame}[fragile]{\currentname}
We can do this with the following:

\begin{minted}[breaklines,fontsize=\scriptsize]{yaml}
    stages:
     - lint
     - test

    lint:phpcs:
      stage: lint
      image: php:alpine
      script:
        - composer install
        - vendor/bin/phpcs
\end{minted}
\end{frame}

% }}}

% sub:where_to_go_from_here_ {{{
\subsection{Where to go from here?}
\label{sub:where_to_go_from_here_}

\begin{frame}[t]{\currentname}
	Currently we've only got linting, unit tests and dependency checking going on. But we have integration and acceptance tests, too.
\end{frame}

% }}}

% }}}

% sec:workshop {{{
\section{Workshop}
\label{sec:workshop}

% sub:task_integrate_gitlab_ci_into_one_of_your_projects {{{
\subsection{Task: Integrate GitLab CI into One of Your Projects}
\label{sub:task_integrate_gitlab_ci_into_one_of_your_projects}

\begin{frame}[t]{}
	\vspace*{\fill}
	\begin{cardTitle}{\currentname}
	As a way of getting started with GitLab yourself, I'd recommend taking a project of yours, and integrating GitLab CI into it.
	\end{cardTitle}
\end{frame}

% }}}

% sub:task_add_security_tests_for_your_own_code {{{
\subsection{Task: Add security tests for your own code}
\label{sub:task_add_security_tests_for_your_own_code}

\begin{frame}[t]{}
	\vspace*{\fill}
	\begin{cardTitle}{\currentname}
	Add a static analysis tool to your pipeline to ensure your own code is secure, too.
	\end{cardTitle}
\end{frame}

% }}}

% sub:task_add_code_coverage_metrics_to_the_application {{{
\subsection{Task: Add Code Coverage Metrics to the Application}
\label{sub:task_add_code_coverage_metrics_to_the_application}

\begin{frame}[t]{}
	\vspace*{\fill}
	\begin{cardTitle}{\currentname}
	For this task, configure code coverage collection for your given application, and hook it into GitLab, so it can report on commits, MRs, etc.
	\end{cardTitle}
\end{frame}
% }}}

% sub:hey_these_dependencies_take_a_while_to_download {{{
\subsection{Task: Hey, These Dependencies Take a While to Download}
\label{sub:task_hey_these_dependencies_take_a_while_to_download}

\begin{frame}[t]{}
	\vspace*{\fill}
	\begin{cardTitle}{\currentname}
	Add a Docker image that bundles all the dependencies, that is hosted within the project's private registry.

	Hint: You can check out \href{https://gitlab.com/jamietanna/jvt.me}{the code for my personal website} for an idea of how to do this.
	\end{cardTitle}
\end{frame}
% }}}

% }}}

\begin{frame}[allowframebreaks]
	\frametitle{References}

	\bibliography{gitlab-kickstarting-ci.bib}
	\bibliographystyle{abbrv}
\end{frame}

\end{document}
