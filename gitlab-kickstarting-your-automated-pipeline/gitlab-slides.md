# what is GitLab

- Brief intro on actually what it is
- Just overview, maybe brief "why you should use it anyway"

# But I currently use `x`, why would I bother with GitLab CI?

- Info from the `why-gitlab` article

##

# Converting an existing application

Instead of us just talking about having a very basic application and then adding GitLab to it, let's take an existing application with things it's doing, and we'll give it some tests.
