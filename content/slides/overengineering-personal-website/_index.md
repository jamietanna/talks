+++
outputs = ["Reveal"]
title = "Overengineering Your Personal Website - How I Learn Things Best"
url = "/overengineering-personal-website/slides/"
[reveal_hugo]
auto_slide = 15000
+++
