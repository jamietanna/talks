---
weight: 20
---
{{% section %}}
# Building Blocks

{{% note %}}
adopt individually, not the whole stack
{{% /note %}}

---

## Identity

{{% note %}}
having a personal website - already owning identity
markup with MF2
/elsewhere
{{% /note %}}

---

## Posts

{{% note %}}
_the_ building block
notes/articles/comments
{{% /note %}}

---

## Citability

{{% note %}}
limited character count to link to others
{{% /note %}}

---

## Syndication

{{% note %}}
post on your site, push elsewhere
also backfeed
{{% /note %}}

---

## Mentions

{{% note %}}
let someone else know you've mentioned them
{{% /note %}}

---

## Login

{{% note %}}
use domain to authN
{{% /note %}}

---

## Web Actions

{{% note %}}
interface/UX of discrete action i.e. like, reply
{{% /note %}}

---

## Reply Context

{{% note %}}
show what you're replying to!
{{% /note %}}

---

## Link Preview

{{% note %}}
show more info from _any link_
{{% /note %}}

{{% /section %}}
