---
weight: 2
---
{{% section %}}
{{< slide transition="fade-out" transition-speed="fast" >}}

<h1 style="font-size: 2em">The IndieWeb Movement: Owning Your Data and Being the Change You Want to See in the Web</h1>

Jamie Tanna (https://www.jvt.me)

{{% note %}}
<h1>Clicker works!</h1>
{{% /note %}}


---

## `/usr/bin/whoami`

- Jamie Tanna
- https://www.jvt.me
- @JamieTanna on Twitter
- He/him

{{% note %}}
- serial blogger
- serial tinkerer
- pronouns
{{% /note %}}

---

## Outline

<ul>
{{% fragment %}}<li>What is the IndieWeb?</li>{{% /fragment %}}
{{% fragment %}}<li>What is the indieweb?</li>{{% /fragment %}}
{{% fragment %}}<li>Principles</li>{{% /fragment %}}
{{% fragment %}}<li>Building Blocks + Ecosystem</li>{{% /fragment %}}
{{% fragment %}}<li>Getting Started</li>{{% /fragment %}}
</ul>

---

![Screenshot of title slide](/indieweb/img/tn-lightning.png)

{{% note %}}
- 2018 reading on holiday, 2019 involved
{{% /note %}}

---

<img src="/indieweb/img/jvtme.png" style="max-width: 70%"/>

{{% /section %}}
