---
weight: 25
---
{{% section %}}

# Indie Ecosystem

{{% note %}}
There's a tonne of technologies and standards that make up the Indie ecosystem, which I thought I'd explain a little more about.
{{% /note %}}

---

## Microformats2

{{% note %}}
Microformats successor
semantic HTML
CSS classes to markup data in structured way, without impacting human-readable
hide with CSS what isn't needed
{{% /note %}}

---

{{< slide class="code-medium" >}}

```html
<div class="h-entry">
  <span class="p-author h-card">
    <a class="u-url" href="https://www.jvt.me">
      <img class="u-photo"
        src="https://www.jvt.me/profile.png" alt=""/>
      Jamie Tanna
    </a>
  </span>:
  RSVP <span class="p-rsvp">yes</span>
  to <a href="https://indiewebcamp.nl/"
    class="u-in-reply-to">IndieWebCamp Amsterdam</a>
</div>
```

<span>
  <a class="u-url" href="https://www.jvt.me">
    <img style="height: 32px; width: 32px; margin: 0; display: inline-block" src="https://www.jvt.me/img/profile.png" alt=""/>
    Jamie Tanna
  </a>
</span>:
RSVP yes
to <a href="https://indiewebcamp.nl/">IndieWebCamp Amsterdam</a>

---

## Micropub

[micropub.spec.indieweb.org](https://micropub.spec.indieweb.org/)

{{% note %}}
used to have a very specific workflow for long-form articles, including local build, branches
doesn't scale to a "I like that post" on i.e. Twitter (JSON files!) which happens from a mobile device
standardised CRUD API
{{% /note %}}

---

## Microsub

[microsub.spec.indieweb.org](https://microsub.spec.indieweb.org/)

---

<span style="font-size: 3em">{{% emojify ":older_man:" %}}</span>

{{% note %}}
RSS feeds
{{% /note %}}

---

### Server

{{% note %}}
- What channels do I have?
- What content in that channel?
- What's the latest content?
- What content types am I parsing?
{{% /note %}}

---

### Client

{{% note %}}
"just render it"

{{% /note %}}

---

Won't it just go the way of RSS?

{{% note %}}
RSS was lonely
hard to share
looking to orient around "follow this person", rather than "follow this feed"
{{% /note %}}

---

## Webmention

{{% note %}}
"Social" in Social Media
discovery of interactions
{{% /note %}}

---

{{< slide class="code-medium" >}}

```
POST /webmention-endpoint HTTP/1.1
Host: aaronpk.example
Content-Type: application/x-www-form-urlencoded

source=https://waterpigs.example/post-by-barnaby&
target=https://aaronpk.example/post-by-aaron
```
---

## Brid.gy

<ul>
{{% fragment %}}<li>backfeed</li>{{% /fragment %}}
{{% fragment %}}<li>syndicate</li>{{% /fragment %}}
</ul>

{{% note %}}
bridge to the silo web

Twitter backfeed
POSSE bookmarks
{{% /note %}}

---

## Web Sign In / IndieAuth

{{% note %}}
OAuth2 as authN
but need pre-registration - can't scale!
{{% /note %}}

---

![Text box requesting URL of user signing in](/indieweb/img/web-sign-in.png)

---

![Authorization server requesting OAuth2 consent for new sign-in](/indieweb/img/authz-server.png)

---

{{< slide class="code-medium" >}}

```http
HTTP/2 200
server: nginx/1.14.0
content-type: text/html; charset=UTF-8
cache-control: no-cache
link: <https://aaronparecki.com/auth>;
  rel="authorization_endpoint"
link: <https://aaronparecki.com/micropub>;
  rel="micropub"
link: <https://aaronparecki.com/auth/token>;
  rel="token_endpoint"
link: <https://aaronparecki.com/>; rel="self"
...
date: Wed, 04 Sep 2019 17:44:15 GMT
strict-transport-security: max-age=2592000
x-no-cache: 1
x-cache: BYPASS
```

---

{{< slide class="code-large" >}}

```html
<link rel="authorization_endpoint"
  href="https://indieauth.com/auth" />
<link rel="token_endpoint"
  href="https://tokens.indieauth.com/token" />
```

---

{{< slide class="code-medium" >}}

### Rel-Me Auth

```html
<a rel="me" href="https://gitlab.com/jamietanna">
  <i class="fa fa-gitlab" title="GitLab.com Profile"></i>
  &nbsp;@jamietanna
</a>
```

{{% /section %}}
