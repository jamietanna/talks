---
weight: 15
---
{{% section %}}
# Principles

---

## Own Your Data

{{% note %}}
not always public
{{% /note %}}

---

<span style="font-size: 3em">{{% emojify ":calendar:" %}}</span>

{{% fragment %}}https://www.jvt.me/kind/rsvps/ - currently 308 in total!{{% /fragment %}}

{{% fragment %}}https://www.jvt.me/rsvps/index.ics{{% /fragment %}}

{{% note %}}
biggest benefit for me was owning my RSVPs
attend lots of tech meetups
used to use Meetup.com calendar integration, but no historical data
308 includes the past
{{% /note %}}

---

- Step count
- Photos
- Diet
- Gaming
- Listening
- Watching

---

## Use & Publish Visible Data

<ol>
  {{% fragment %}}<li>human readable</li>{{% /fragment %}}
  {{% fragment %}}<li>machine readable</li>{{% /fragment %}}
</ol>

---

### Microformats

---

## Make What You Need / Scratch Your Itch

---

### Solve _your_ problems, not someone else's

{{% note %}}
- don't make generic
- don't trade-off
- imaginary, unhelpful user
- be selfish!
{{% /note %}}

---

### Scratch Your Itch!

---

Homebrew Website Club Nottingham - "when is it again"?

---

<span style="font-size: 3em">{{% emojify ":calendar:" %}}</span>

https://www.jvt.me/events/homebrew-website-club-nottingham/index.ics

---

## Use what you make / Dogfood

{{% note %}}
Why would someone else use it?
Use it yourself
Build confidence by day-in, day-out
{{% /note %}}

---

## Document Stuff

<img class="fragment" style="width: 50%; height: 50" src="/indieweb/img/blog-about-it.png" alt="\"That's so interesting. I shall go write about it in my blog\"" />

<ul>
{{% fragment %}}<li>Let others know you've done it</li>{{% /fragment %}}
{{% fragment %}}<li>Help/motivate someone else</li>{{% /fragment %}}
{{% fragment %}}<li>Learn by documenting/teaching</li>{{% /fragment %}}
{{% fragment %}}<li>{{% emojify ":duck:" %}}</li>{{% /fragment %}}
</ul>

{{% note %}}
This, but unsarcastically
{{% /note %}}

---

## Share under Free/Open Source Licenses

{{% note %}}
not a requirement
scary to put things out
use, improve, learn
{{% /note %}}

---

## UX/Design is better than technical specifications

{{% note %}}
more implementations before standardise
{{% /note %}}

---

## Modularity

"platform agnostic platform"

"small pieces loosely joined"

{{% note %}}
unix philosophy
swap out pieces of code / the stack to move to new, interesting things
re-use, different tech stacks, improve over time
{{% /note %}}

---

## Longevity

{{% note %}}
can we build for the "long Web" i.e. 5, 10, 20 years of use?
{{% /note %}}

---

## Plurality

{{% note %}}
common in FOSS to want to pool resources
actively encouraged
multiple implementations
prevent monocultures
{{% /note %}}

---

## Fun

---

## Don't force others to move

{{% fragment %}}Publish (on your) Own Site, Syndicate Elsewhere (POSSE){{% /fragment %}}

{{% note %}}
- Not official

> To follow my stuff you need an Indie Reader, which requires MicroSub, which requires IndieAuth ...

- Unfair to force, dragged heels

- POSSE retains ownership, but includes others in the conversation
{{% /note %}}

---

### Brid.gy

{{% /section %}}
