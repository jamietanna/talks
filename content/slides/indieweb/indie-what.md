---
weight: 10
---
{{% section %}}
# What is the IndieWeb?

{{% note %}}
- name suggests, about the web
- revolves around personal domain
- indie = independent
{{% /note %}}

---

## IndieWeb vs indieweb

Via https://theadhocracy.co.uk/wrote/one-year-in-the-indieweb (emphasis mine):

<div style="font-size: 70%">

<blockquote style="width: 90%">
<ul>
<li>
Have a website;
</li>
<li>
Own your URL;
</li>
<li>
Make it available.
</li>
</ul>

<p>
Those three steps are what you need to do in order to be part of the <strong>independent, or indie, web</strong>. But that's lowercase i, lowercase w. The IndieWeb – check that casing – is a bit more specific than that. <strong>The IndieWeb is an attempt at moving behaviours and functionality away from large, corporately-owned online communities</strong> (what people involved in the IndieWeb refer to as "silos") and onto independent websites. Specifically, the IndieWeb is focused on online social behaviour.
</p>
</blockquote>

</div>

{{% note %}}
- Murray Adcock
- an important way to view the IndieWeb
{{% /note %}}

---

## As a toolkit

Via https://theadhocracy.co.uk/wrote/one-year-in-the-indieweb:

<blockquote style="font-size: 60%; width: 110%">
<p>
Maybe you're trying to create a consistent experience across different digital channels, so that if someone likes a Tweet about your article then that "like" shows up when on the article? Sounds like you need to implement backfeed
</p>
<p>
Or maybe you want an easy way for people to comment on a page without needing to sign in? Webmentions will probably help you out
</p>
<p>
Looking for a way to consume RSS that feels a bit more like a two-way conversation? Check out social readers
</p>
<p>
Want to easily publish ideas directly from your phone to your website? Micropub might be what you're after
</p>
</blockquote>

---

## Ownership

<ul>
{{% fragment %}}<li>Own your identity</li>{{% /fragment %}}
{{% fragment %}}<li>Own your data</li>{{% /fragment %}}
{{% fragment %}}<li>On your site _first_</li>{{% /fragment %}}
{{% fragment %}}<li>On your own infrastructure?</li>{{% /fragment %}}
</ul>

{{% note %}}
- identity is the important one
  - could be on Tumblr, could be WordPress, could be own - but regardless, people come to `www.jvt.me`
- what data? Thoughts, workout, art
- POSSE
{{% /note %}}

---

## Break free from silos

- "Open"
- ownership of data


{{% note %}}
- own your content? if not, it's their URL, not yours
- Open leads to being user hostile / locked down, inaccessible
{{% /note %}}

---

Via https://bradfrost.com/blog/post/write-on-your-own-website/:

<blockquote style="width: 90%" class="fragment">Writing on your own website associates your thoughts and ideas with you as a person. Having a distinct website design helps strengthen that association. Writing for another publication you get a little circular avatar at the beginning of the post and a brief bio at the end of the post, and that’s about it. People will remember the publication, but probably not your name.</blockquote>


{{% note %}}
- some don't even give you rights! i.e. Baeldung
{{% /note %}}

---

Shouldn't you get a choice of what to do with your content?

<ul>
{{% fragment %}}<li><a href="https://uk.pcmag.com/news/123100/still-use-yahoo-groups-content-will-be-deleted-on-dec-14">Yahoo Groups' user content is being deleted 3 months from announcement</a></li>{{% /fragment %}}
{{% fragment %}}<li><a href="https://www.washingtonpost.com/business/2018/12/04/tumblrs-nudity-crackdown-means-pornography-will-be-harder-find-its-platform-than-nazi-propaganda/">Tumblr's recent-ish ban on pornography</a></li>{{% /fragment %}}
{{% fragment %}}<li><a href="https://www.theverge.com/2018/7/1/17521456/500px-marketplace-creative-commons-getty-images-visual-china-group-photography-open-access">500px no longer allowing Creative Commons licensing</a></li>{{% /fragment %}}
{{% fragment %}}<li><a href="https://spectrum.chat/spectrum/general/join-us-on-our-new-journey~e4ca0386-f15c-4ba8-8184-21cf5fa39cf5">Spectrum Community Chat being sunset after acquisition by GitHub</a></li>{{% /fragment %}}
{{% fragment %}}<li><a href="https://indieweb.org/site-deaths">IndieWeb.org: Site Deaths</a></li>{{% /fragment %}}

</ul>

---

<h1>{{% emojify ":moneybag:" %}} {{% emojify ":eyes:" %}}</h1>

{{% note %}}
- cash is more important than users' privacy, happiness
- at least they'll want your attention
- accessibility, dark UX patterns
{{% /note %}}

---

## Be Your Own Platform

<ul>
{{% fragment %}}<li>guidelines</li>{{% /fragment %}}
{{% fragment %}}<li>revolve it around you!</li>{{% /fragment %}}
{{% fragment %}}<li>make the changes you want/need</li>{{% /fragment %}}
{{% fragment %}}<li>support from the community</li>{{% /fragment %}}
{{% fragment %}}<li>don't just accept the state of play</li>{{% /fragment %}}
</ul>

{{% note %}}
- third party app removal i.e. twitter
{{% /note %}}

---

## A Political Statement

{{% note %}}
just like how FOSS is, saying "we don't want to use your platform" is a statement
{{% /note %}}

{{% /section %}}
