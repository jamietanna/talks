---
weight: 50
---
{{% section %}}
# Conclusion

- indieweb - have a website on a URL
- IndieWeb - more political, replace silos with Open, Social, platforms
- Existing tooling, standards, and open discussion to support new ways of doing everything

---

# Getting Started

- https://indieweb.org/next-hwc
- https://indieweb.org/Getting_Started
- https://indieweb.org/discuss
- https://indieweb.org/2021
- https://indiewebify.me/
- https://withknown.com/
- https://micro.blog
- https://www.jvt.me/tags/indieweb/
- https://www.jvt.me/posts/2019/07/22/why-website/
- https://www.jvt.me/posts/2019/10/20/indieweb-talk/

{{% note %}}
Thank you - any questions?
{{% /note %}}
{{% /section %}}
