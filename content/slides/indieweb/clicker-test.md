---
weight: 1
---
{{% section %}}
{{< slide transition="fade-out" transition-speed="fast" >}}

<h1 style="font-size: 2em">The IndieWeb Movement: Owning Your Data and Being the Change You Want to See in the Web</h1>

Jamie Tanna (https://www.jvt.me)
{{% /section %}}
