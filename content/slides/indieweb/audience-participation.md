---
weight: 5
---
{{% section %}}

<h2>{{% emojify ":rotating_light:" %}} Audience Participation {{% emojify ":rotating_light:" %}}</h2>

<ul style="list-style-type: none">
{{% fragment %}}<li>{{% emojify ":raised_hand:" %}} if you post content on Twitter/Medium/Dev.to/etc</li>{{% /fragment %}}
{{% fragment %}}<li>{{% emojify ":raised_hand:" %}} if you have a personal website</li>{{% /fragment %}}
{{% fragment %}}<li>{{% emojify ":raised_hand:" %}} if you post content on your personal website</li>{{% /fragment %}}
{{% fragment %}}<li>{{% emojify ":raised_hand:" %}} if you've heard about the IndieWeb before / know what it's about</li>{{% /fragment %}}
</ul>

---

Why are you here?

{{% note %}}

- at the conference - want to live in a collaborative world where we can learn, grow from each other
- surely want something better than the current world in terms of how the Web works?
{{% /note %}}

---

<blockquote style="width: 90%">
We strive for Free and Open Source software in the world, on mobile, desktop and server. But what about the Web? The IndieWeb is all about taking control, owning your data, and scratching your itches through Open Source and Standards. We're working to take back the Web, and you can, too.
</blockquote>

{{% /section %}}
