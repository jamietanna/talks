+++
outputs = ["Reveal"]
title = "The IndieWeb Movement: Owning Your Data and Being the Change You Want to See in the Web"
url = "/indieweb/slides/"
[author]
[author.website]
link = "https://www.jvt.me"
name = "www.jvt.me"
[author.twitter]
link = "https://twitter.com/jamietanna"
name = "@JamieTanna"
+++
