---
---
{{% section %}}

## Why is it important?

{{% note %}}
{{% /note %}}

{{% /section %}}


{{% section %}}

As I wrote in the post [_Analysing our dependency trees to determine where we should send Open Source contributions for Hacktoberfest_](https://www.jvt.me/posts/2022/09/29/roo-hacktoberfest-dependency-analysis/)

{{% note %}}
- recent years, unavoidable build software on top of OSS
- absolutely a great thing, reduce focus on domain specialisation
- pick up on defects, add features
{{% /note %}}

{{% /section %}}

{{% section %}}

But it's not always ☀🌈

<div class=fragment>

![xkcd comic showing a tower of various layers of boulders and stones, labelled "all modern digital infrastructure", which looks a little precarious. Towards the bottom there is a slim load-bearing stone which is labelled "a project some random person in Nebraska has been thanklessly maintaining since 2003"](https://imgs.xkcd.com/comics/dependency.png)

</div>

{{% note %}}
- not always smooth sailing
- don't know the impact of a library on your supply chain
- Log4Shell, colors.js and faker.js
- chef library ICE
{{% /note %}}

{{% /section %}}

{{% section %}}

<span style="font-size: 250%">💖</span>

{{% note %}}
- Open Source projects need support, love and positive feedback from their communities
{{% /note %}}

{{% /section %}}

{{% section %}}

Do you fully appreciate the depth of your dependency on the software supply chain?

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

Being able to understand how your business uses Open Source is really important for a few other key reasons:

<ul>
<li class=fragment>
How am I affected by <em>that dependency</em> migrating away from Open Source?
</li>
<li class=fragment>
Usages of unwanted libraries
</li>
<li class=fragment>
Understand usage of libraries and frameworks, and their versions
</li>
<li class=fragment>
Discovering unmaintained, deprecated or vulnerable software
</li>
</ul>

{{% note %}}
unwanted - copyleft, not recommended any more

{{% /note %}}

{{% /section %}}


{{% section %}}

Being able to understand how your business uses ~~Open Source~~ internal software is really important for a few other key reasons:

<ul>
<li>
<s>How am I affected by <em>that dependency</em> migrating away from Open Source?</s>
</li>
<li>
Usages of unwanted libraries
</li>
<li>
Understand usage of libraries and frameworks, and their versions
</li>
<li>
Discovering unmaintained, deprecated or vulnerable software
</li>
</ul>

{{% note %}}
although the tagline for this talk is OSS, it's super important to know internally, too.

i.e. are you depending on a really old version of an internal security library?

{{% /note %}}

{{% /section %}}

{{% section %}}

Other insights into:

<ul>
<li class=fragment>
How maintained does the dependency appear to be?
</li>
<li class=fragment>
How are the dependency's supply chain security practices? (via OpenSSF Security Scorecards)
</li>
<li class=fragment>
How many dependencies are actively seeking financial support?
</li>
</ul>

{{% /section %}}


{{% section %}}

How can we do it?

<p style="text-align: centre" class=fragment>
💰🤑💸
</p>

<p style="text-align: centre" class=fragment>
    <!-- forgive my sins -->
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    <!-- /forgive -->
    <img style="height: 64px; display: inline-block; margin: auto;" src=https://edent.github.io/SuperTinyIcons/images/svg/github.svg alt="GitHub logo">
    <img style="height: 64px; display: inline-block; margin: auto;" src=https://edent.github.io/SuperTinyIcons/images/svg/gitlab.svg alt="GitLab logo">
    <img style="height: 64px; display: inline-block; margin: auto;" src=https://res.cloudinary.com/snyk/image/upload/v1537345891/press-kit/brand/avatar-transparent.png alt="Snyk logo">
    <img style="height: 64px; display: inline-block; margin: auto;" src=https://www.mend.io/wp-content/media/2023/04/mend.io.logo.dark.png alt="Mend logo">
</p>

{{% note %}}

This is an area dominated by big players - did you want to shell out a load of money?
{{% /note %}}

{{% /section %}}

{{% section %}}

Let's use Open Source!

<img style="height: 128px" src=https://edent.github.io/SuperTinyIcons/images/svg/opensource.svg alt="Open Source Initiative logo">

{{% note %}}

No, let's look at it using Open Source and Open APIs
{{% /note %}}

{{% /section %}}
