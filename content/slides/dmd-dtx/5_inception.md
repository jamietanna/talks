---
---
{{% section %}}

## How did it come to be?

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

[_Idea for Open Source/Startup: monetising the supply chain_](https://www.jvt.me/posts/2022/06/01/idea-supply-chain-monetisation/)

{{% note %}}

- C1, side project deps using, understand what needs upgrade
- Rolled out Mend Renovate, no longer needed
- I suggested understand to distribute (financial) support
- before that, looked at understanding usage + versions of internal libraries

{{% /note %}}

{{% /section %}}

{{% section %}}

[_Analysing our dependency trees to determine where we should send Open Source contributions for Hacktoberfest_](https://www.jvt.me/posts/2022/09/29/roo-hacktoberfest-dependency-analysis/)

<ul>
<li class=fragment>
Using the Dependabot APIs
</li>
<li class=fragment>
Good starting point
</li>
<li class=fragment>
Lack of data for some ecosystems
</li>
<li class=fragment>
Hard to parse the "current version"
</li>
</ul>


{{% note %}}
- Hacktoberfest 2022, wanted to get more Roo folks involved
- "what should I contribute to"
- data driven company 👉🏼 decided to get some data for it
- rolled out GitHub Advanced Security, Dependabot
- take data and give opportunity to find fairly well-used, but maybe not well-contributed, libraries

{{% /note %}}

{{% /section %}}

{{% section %}}

<iframe src="https://giphy.com/embed/xT5LMY7hQj7OtAZ6G4" width="480" height="366" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/season-11-the-simpsons-11x6-xT5LMY7hQj7OtAZ6G4">via GIPHY</a></p>

{{% note %}}
Not a one trick pony - DataDog, continually finding use cases
{{% /note %}}

{{% /section %}}

{{% section %}}

![Mend Renovate logo](https://app.renovatebot.com/images/whitesource_renovate_660_220.jpg)

{{% note %}}
Renovate rollout, better data i.e. sbt, CircleCI
{{% /note %}}

{{% /section %}}

{{% section %}}

![EndOfLife.date logo](https://media.jvt.me/5e4284dba9.png)

{{% note %}}
what is it?

opportunity to finalise DB structure

{{% /note %}}

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

```sh
commit 73a99614a2af6fa9f66508bab8541ed65e18ed66
Author: Jamie Tanna <>
Date:   Thu Feb 2 09:23:43 2023 +0000

    Initialise project

 LICENSE.md        | 13 +++++++++++++
 README.md         |  7 +++++++
 public/index.html | 90 ++++++++++++++++++++++++++++++++++++
 3 files changed, 110 insertions(+)
```

</div>

{{% note %}}

- wanted to roll it out as its own project
- passionate (evenings/weekends)
- wanted to be able to use it wherever I went

{{% /note %}}

{{% /section %}}
