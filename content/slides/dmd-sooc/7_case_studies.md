---
---

{{% section %}}

## Case Studies

https://dmd.tanna.dev/case-studies/

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

"What package advisories do I have?"

<table class="rows-and-columns" style="font-size: 50%; margin-left: -100px">
<!-- <table class="rows-and-columns" style="font-size: 50%"> -->

<thead>
<tr>
<th class="col-organisation" scope="col">organisation</th><th class="col-repo" scope="col">repo</th><th class="col-package_name" scope="col">package_name</th><th class="col-version" scope="col">current_version</th><th class="col-dep_types" scope="col">dep_types</th><th class="col-advisory_type" scope="col">advisory_type</th><th class="col-description" scope="col">description</th>
</tr>
</thead>
<tr>

<td class="col-organisation">alphagov</td>

<td class="col-repo">pay-selfservice</td>

<td class="col-package_name">node</td>

<td class="col-version">18.17.1</td>

<td class="col-dep_types">["engines"]</td>

<td class="col-advisory_type">DEPRECATED</td>

<td class="col-description">nodejs 18 has been unsupported (usually only receiving critical security fixes) for 107 days</td>

</tr>

<tr>

<td class="col-organisation">golangci</td>

<td class="col-repo">golangci-lint</td>

<td class="col-package_name">github.com/golangci/lint-1</td>

<td class="col-current_version">v0.0.0-...</td>

<td class="col-dep_types">["require"]</td>

<td class="col-advisory_type">DEPRECATED</td>

<td class="col-description">Use golang.org/x/lint instead, as the golangci fork ... is behind active development and bugfixes in golang.org/x/lint</td>

</tr>

<tr>

<td class="col-organisation">monzo</td>

<td class="col-repo">response</td>

<td class="col-package_name">python</td>

<td class="col-version">3.7</td>

<td class="col-dep_types">[]</td>

<td class="col-advisory_type">UNMAINTAINED</td>

<td class="col-description">python 3.7 has been End-of-Life for 220 days</td>

</tr>

</table>

<small>via https://dependency-management-data-example.fly.dev/report/advisories</small>

{{% note %}}

{{% /note %}}

{{% /section %}}

{{% section %}}

### Which other services may be affected by this production bug?

https://dmd.tanna.dev/case-studies/deliveroo-kafka-sidecar/

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

```yaml
# there may be some folks using YAML anchors
sidecars: &sidecars
    # or there could also be comments in here!
    kafka: 1.2.3

app:
    image: "internal.docker.registry/service-name"
    *sidecars
```

</div>

{{% note %}}
- out-of-date library caused incident - how can we work out other affected services?
- hard without DMD - GitHub query
- straightforward with DMD!
{{% /note %}}


{{% /section %}}

{{% section %}}

In the `renovate` table:

<div style="font-size: 80%">

|         repo          |       package_name        | current_version | package_file_path |
|-----------------------|---------------------------|---------|-------------------|
| good-service          | internal-docker.tld/kafka | 0.3.0   | .hopper.yml       |
| affected-service      | internal-docker.tld/kafka | 0.2.1   | .hopper.yml       |
| also-affected-service | internal-docker.tld/kafka | 0.1.0   | .hopper.yml       |

<small>Parsed via https://docs.renovatebot.com/modules/manager/regex/</small>

</div>

{{% /section %}}

{{% section %}}

And the `repository_metadata` table:

|         repo          | additional_metadata  |
|-----------------------|----------------------|
| good-service          | `{"tier": "tier_1"}` |
| affected-service      | `{"tier": "tier_1"}` |
| also-affected-service | `{"tier": "tier_2"}` |

{{% /section %}}

{{% section %}}

Slightly garish query:

<div style="font-size: 120%">

```sql
select
  renovate.organisation,
  renovate.repo,
  current_version,
  owner,
  json_extract(additional_metadata, '$.tier') as tier
from
  renovate
  left join owners on
      renovate.platform     = owners.platform
  and renovate.organisation = owners.organisation
  and renovate.repo         = owners.repo
  left join repository_metadata on
      renovate.platform     = repository_metadata.platform
  and renovate.organisation = repository_metadata.organisation
  and renovate.repo         = repository_metadata.repo
where
  -- NOTE: that this is performed with a lexicographical match, which is NOT
  -- likely to be what you are expecting to perform version constraint matching
  -- but this is a good start for these use cases
  renovate.current_version < '0.3'
order by
  tier ASC
```

</div>

{{% /section %}}

{{% section %}}

Result:

| organisation |         repo          | current_version |  owner  |  tier  |
|--------------|-----------------------|-----------------|---------|--------|
| deliveroo    | affected-service      | 0.2.1           | Grocery | tier_1 |
| deliveroo    | also-affected-service | 0.1.0           |         | tier_2 |



{{% /section %}}

{{% section %}}

### Log4shell

https://dmd.tanna.dev/case-studies/log4shell/

{{% note %}}
Before DMD existed, but a great example
An interesting one, many ways to do this
{{% /note %}}

{{% /section %}}

{{% section %}}

We could use the `dependenton` query:

```
# for Gradle projects
$ dmd report dependenton --db dmd.db --package-manager gradle
  --package-name org.apache.logging.log4j:log4j-core
+-------------------+---------+-------------------+----------------------------+-------------+
| REPO              | VERSION | DEPENDENCY TYPES  | FILEPATH                   | OWNER       |
+-------------------+---------+-------------------+----------------------------+-------------+
| logstash          | 2.17.1  | ["dependencies"]  | logstash-core/build.gradle | Elastic     |
| logstash          | 2.17.1  | ["dependencies"]  | logstash-core/build.gradle | Elastic     |
| fake-private-repo | 2.13.0  | ["dependencies"]  | blank-java/build.gradle    | Jamie Tanna |
| fake-private-repo | 2.13.0  | ["dependencies"]  | blank-java/build.gradle    | Jamie Tanna |
+-------------------+---------+-------------------+----------------------------+-------------+
```

{{% note %}}
Also available via <code>dmd-web</code> or GraphQL
{{% /note %}}

{{% /section %}}

{{% section %}}

Or start with SQL:

```sql
select
  platform,
  organisation,
  repo,
  current_version
from
  renovate
where
  package_name = 'org.apache.logging.log4j:log4j-core'
```

{{% /section %}}

{{% section %}}

With the versions affected:

```sql
select
  platform,
  organisation,
  repo,
  current_version
from
  renovate
where
  package_name = 'org.apache.logging.log4j:log4j-core'
  and current_version in (
    '2.0-beta9',	'2.0-rc1',
    '2.0-rc2',		'2.0.1',
    '2.0.2',		'2.0',
    -- ....
    -- ....
    -- ....
    '2.13.0',		'2.13.1',
    '2.13.2',		'2.13.3',
    '2.14.0',		'2.14.1',
  )
```

{{% /section %}}

{{% section %}}

Or with a Policy:

<div style="font-size: 150%">

```go
default advisory_type := "SECURITY"

versions := split(input.dependency.version, ".")
major := to_number(versions[0])
minor := to_number(versions[1])
patch := to_number(versions[2])

is_log4j2 if {
	input.dependency.package_manager in {"gradle", "maven"}
	input.dependency.package_name =
      "org.apache.logging.log4j:log4j-core"

	major == 2
}

// ...
```

</div>

{{% /section %}}

{{% section %}}

Or with Policy (continued):

<div style="font-size: 150%">

```go
// ...

// CVE-2021-44228 aka Log4shell affects versions 2.0-beta9 to
// 2.14.1
is_vulnerable_version if input.dependency.version in {
  "2.0-beta9", "2.0-rc1", "2.0-rc2"}

// CVE-2021-44228 aka Log4shell affects versions 2.0-beta9 to
// 2.14.1
is_vulnerable_version if {
	minor > 0
	minor <= 14
}

deny contains msg if {
	is_log4j2
	is_vulnerable_version
	msg := "Dependency is vulnerable to Log4Shell CVE " +
      "(CVE-2021-44228)"
}
```

</div>

{{% /section %}}

{{% section %}}

### The Gorilla Toolkit archiving

https://dmd.tanna.dev/case-studies/gorilla-toolkit/

{{% note %}}
mux, securecookie, csrf, sessions
{{% /note %}}

{{% /section %}}

{{% section %}}

As of `dmd` v0.26.0, there was an inbuilt query that produced the following output:

```sh
$ dmd report gorillaToolkit --db dmd.db
Renovate
Direct dependencies
+------------------------------+---+
| PACKAGE                      | # |
+------------------------------+---+
| github.com/gorilla/mux       | 4 |
| github.com/gorilla/websocket | 1 |
| github.com/gorilla/handlers  | 1 |
+------------------------------+---+
Indirect dependencies
+---------------------------------+---+
| PACKAGE                         | # |
+---------------------------------+---+
| github.com/gorilla/websocket    | 6 |
| github.com/gorilla/securecookie | 2 |
| github.com/gorilla/sessions     | 1 |
| github.com/gorilla/schema       | 1 |
| github.com/gorilla/mux          | 1 |
| github.com/gorilla/context      | 1 |
+---------------------------------+---+
```

{{% /section %}}

{{% section %}}

Alternatively:

```sql
select
  platform,
  organisation,
  repo,
  arr.value as dep_type
from
  renovate,
  json_each(dep_types) as arr
where
  package_name like 'github.com/gorilla/%'
group by
  package_name
order by
  dep_type desc
```

{{% /section %}}

{{% section %}}

Results in:

<table>
<TR><TH>platform</TH>
<TH>organisation</TH>
<TH>repo</TH>
<TH>dep_type</TH>
</TR>
<TR><TD>github</TD>
<TD>dagger</TD>
<TD>dagger</TD>
<TD>require</TD>
</TR>
<TR><TD>github</TD>
<TD>deepmap</TD>
<TD>oapi-codegen</TD>
<TD>require</TD>
</TR>
<TR><TD>github</TD>
<TD>elastic</TD>
<TD>beats</TD>
<TD>require</TD>
</TR>
<TR><TD>github</TD>
<TD>tailscale</TD>
<TD>tailscale</TD>
<TD>require</TD>
</TR>
<TR><TD>github</TD>
<TD>tailscale</TD>
<TD>tailscale</TD>
<TD>indirect</TD>
</TR>
</table>

{{% /section %}}

{{% section %}}

### Docker free tier sunset

https://dmd.tanna.dev/case-studies/docker-free-sunsetting/

{{% note %}}
not only affect your org, but if pulling others' ("namespaces")
not applied for Docker-Sponsored Open Source Project, or paid up, would be rate limited, or just unusable until next billing cycle
{{% /note %}}

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

```text
$ dmd report mostPopularDockerImages --db dmd.db
+----------------------------------+-----+
| NAMESPACE                        |   # |
+----------------------------------+-----+
| library                          | 499 |
| ghcr.io/gravitational            |  18 |
| dockersamples                    |  12 |
| gcr.io/distroless                |  11 |
| public.ecr.aws/gravitational     |  10 |
| registry1.dsop.io/redhat/ubi     |  10 |
| docker.mirror.hashicorp.services |  10 |
| wiremock                         |   8 |
| docker                           |   8 |
| docker.elastic.co/elasticsearch  |   7 |
| cimg                             |   6 |
| hashicorpdev                     |   6 |
+----------------------------------+-----+
```

</div>

{{% /section %}}
