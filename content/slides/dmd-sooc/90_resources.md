---
---
{{% section %}}

## Getting started

<div style="font-size: 150%">

```sh
# produce some data that DMD can import, i.e.
npx @jamietanna/renovate-graph@latest --token $GITHUB_TOKEN \
  your-org/repo another-org/repo
# or for GitLab
env RENOVATE_PLATFORM=gitlab npx @jamietanna/renovate-graph@latest \
  --token $GITLAB_TOKEN your-org/repo another-org/nested/repo

# set up the database
dmd db init --db dmd.db
# import renovate-graph data
dmd import renovate --db dmd.db 'out/*.json'
# then you can start querying it
sqlite3 dmd.db 'select count(*) from renovate'
```

</div>

https://dmd.tanna.dev/cookbooks/getting-started/

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

## Resources

- https://www.jvt.me/posts/2024/02/06/dmd-talk-sooc/
- https://dmd.tanna.dev/cookbooks/getting-started/
- https://dmd.tanna.dev
- https://talks.jvt.me/dmd/

{{% note %}}
{{% /note %}}

{{% /section %}}
