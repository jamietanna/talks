---
---
{{% section %}}

## What is dependency-management-data?

<img src="/dmd-sooc/logo.svg" alt="" style="height: 200px" />

{{% /section %}}

{{% section %}}

Dependency Management Data (DMD) - [dmd.tanna.dev](https://dmd.tanna.dev)

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

What's in the project?

<ul>
<li class=fragment>
The outputted SQLite database
</li>
<li class=fragment>
The command-line tool <code>dmd</code>
</li>
<li class=fragment>
The web application <code>dmd-web</code>, and the GraphQL-only web application <code>dmd-graph</code>
</li>
<li class=fragment>
(Your SQLite browser of choice)
</li>
</ul>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

SQLite database

<ul>
<li class=fragment>
Conveniently distribute, share
</li>
<li class=fragment>
Great for local-only or building applications on top of it
</li>
<li class=fragment>
No lock-in to <code>dmd</code> - all state synced to the DB
</li>
</ul>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

`dmd`

<ul>
<li class=fragment>
Create the SQLite database
</li>
<li class=fragment>
Ingests different sources of dependency data ("datasources")
</li>
<li class=fragment>
Enrich it with more data ("advisories", "dependency health")
</li>
<li class=fragment>
Configure your own views over the data via "custom advisories" and "policies"
</li>
<li class=fragment>
Provide common queries ("reports")
</li>
</ul>

{{% note %}}
can discover some interesting things without advisories, but it's a game-changer with
{{% /note %}}

{{% /section %}}

{{% section %}}

`dmd-web`

<ul>
<li class=fragment>
Centrally deployable and accessible
</li>
<li class=fragment>
View reports in the browser
</li>
<li class=fragment>
<a href=https://datasette.io>Datasette</a>'s excellent SQLite UI
</li>
<li class=fragment>
GraphQL API
</li>
<li class=fragment>
Great when deployed accessible to all (with authentication)
</li>
<ul>
</ul>
</ul>

{{% note %}}
Share URLs with your colleagues!
{{% /note %}}

{{% /section %}}

{{% section %}}

`dmd-graph`

<ul>
<li class=fragment>
Web application with only the GraphQL API
</li>
<li class=fragment>
Great for internally deployed (for internal API access)
</li>
<ul>
</ul>
</ul>

{{% /section %}}
