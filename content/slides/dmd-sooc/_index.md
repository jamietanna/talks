+++
outputs = ["Reveal"]
title = "Quantifying your reliance on Open Source software"
url = "/dmd-sooc/slides/"
custom_css_partial = "custom-partial/dmd-sooc.css.html"
custom_end_partial = "custom-partial/dmd-sooc.end.html"
[author]
[author.dmd]
link = "https://dmd.tanna.dev"
name = "dmd.tanna.dev"
[author.website]
link = "https://www.jvt.me/elsewhere/"
name = "www.jvt.me/elsewhere"
+++
