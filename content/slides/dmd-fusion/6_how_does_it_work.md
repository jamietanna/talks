---
---

{{% section %}}

## How does it work?

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<div class=code>

```sh
# produce some data that DMD can import, i.e.
npx @jamietanna/renovate-graph@latest --token $GITHUB_TOKEN your-org/repo
# set up the database
dmd db init --db dmd.db
# import renovate-graph data
dmd import renovate --db dmd.db 'out/*.json'
# optionally, generate advisories
dmd db generate advisories --db dmd.db
# then you can start querying it
sqlite3 dmd.db 'select count(*) from renovate'
```

</div>

<style>
div.code .highlight {
font-size: 150%;
}
div.code pre.code-wrapper {
width: 100%;
}
</style>

{{% note %}}
From here, the data is then imported and able to be queried.
{{% /note %}}

{{% /section %}}

{{% section %}}

<div class=code>

```sh
# set up the database
dmd db init --db dmd.db

# taking an SBOM that was produced from the GitLab repo
# https://gitlab.com/tanna.dev/dependency-management-data
dmd import sbom --db dmd.db '/path/to/sbom.json' --platform gitlab
  --organisation tanna.dev
  --repo dependency-management-data
# take an SBOM that was produced in some unknown place,
# and auto-detect what we can
dmd import sbom '/path/to/sbom.json'
# take an SBOM that was produced by a vendor
dmd import sbom '/path/to/sbom.json' --vendor ExampleCorp
  --product 'Web Server' --product-version 5.0.0

# optionally, generate advisories
dmd db generate advisories --db dmd.db
# then you can start querying it
sqlite3 dmd.db 'select count(*) from renovate'
```

</div>

<style>
div.code .highlight {
font-size: 150%;
}
div.code pre.code-wrapper {
width: 100%;
}
</style>

{{% note %}}
From here, the data is then imported and able to be queried.
{{% /note %}}

{{% /section %}}

{{% section %}}

When importing:

- Converts to an underlying data model (in SQLite)
- Uses that for internal querying + enrichment

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

Once ingested, write SQL to your heart's content 🤓 i.e.

<ul>
<li class=fragment>
"which repos use a vulnerable version of Log4J"
</li>
<li class=fragment>
"how many repos are using a version of the Datadog SDK that's older than ..."
</li>
<li class=fragment>
"what is our most used direct/transitive dependency?"
</li>
</ul>

{{% /section %}}

{{% section %}}

### Advisories

{{% note %}}
{{% /note %}}

{{% /section %}}


{{% section %}}

Right now we can write SQL queries to ask:

<ul>
<li class=fragment>

_what Terraform modules and versions are being used across the org_?


</li>
<li class=fragment>

_which teams are using the Gin web framework_?

</li>
</ul>

{{% note %}}
with the DB access we get out-of-the-box ...
{{% /note %}}

{{% /section %}}

{{% section %}}

But what if we could ask:

<ul>
<li class=fragment>

_are any of our projects relying on libraries that no longer are recommended by our language guilds_?

</li>
<li class=fragment>

_how much time should my team(s) be planning in the next quarter to upgrade their AWS infrastructure_?

</li>
</ul>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<iframe src="https://giphy.com/embed/lrc1TZHRYxj7lGM3Vg" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/NoraFromQueens-awkwafina-is-nora-from-queens-lrc1TZHRYxj7lGM3Vg">via GIPHY</a></p>

{{% note %}}
I need more data
{{% /note %}}

{{% /section %}}

{{% section %}}

Dependency advisory data sources:

- [endoflife.date](https://endoflife.date)
- [deps.dev](https://deps.dev)
- [ecosyste.ms](https://ecosyste.ms)

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

AWS infrastructure advisory data sources:

- [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker/)

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

🤫

{{% note %}}
Don't wanna share internal things publicly

{{% /note %}}

{{% /section %}}

{{% section %}}

Custom advisories 🦸

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

Community provided advisories via [-contrib](https://gitlab.com/tanna.dev/dependency-management-data-contrib):

```sql
INSERT INTO custom_advisories (
  package_pattern,
  package_manager,
  version,
  version_match_strategy,
  advisory_type,
  description
) VALUES (
  'github.com/golang/mock',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'golang/mock is no longer maintained, and active development been moved to github.com/uber/mock'
);
```

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

### Policies

{{% note %}}
Alternatively, for a much more powerful way to write queries
{{% /note %}}

{{% /section %}}

{{% section %}}

<img src=http://www.openpolicyagent.org/img/opa-horizontal-color.png alt="Open Policy Agent logo" />

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

<!-- TODO: update to `rego` once released -->

```go
// Versions of Gin >= 1.9
deny contains msg if {
	input.dependency.package_manager in {"gomod", "golang"}
	input.dependency.package_name = "github.com/gin-gonic/gin"
	versions[0] in {"v1", "1"}
	to_number(versions[1]) >= 9
	msg := sprintf("%s. Versions of Gin since v1.9.0 have shipped " +
      "ByteDance/sonic as an optional dependency, but it still " +
      "appears as a dependency, and could be in use - more " +
      "details in " +
      "https://github.com/gin-gonic/gin/issues/3653", [prefix])
}


```

</div>

{{% note %}}
{{% /note %}}

{{% /section %}}
