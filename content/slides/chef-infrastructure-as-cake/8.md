---
weight: 9
---
{{% section %}}
## Review

- {{% fragment %}}A Crash Course in Configuration Management{{% /fragment %}}
- {{% fragment %}}Writing Cookbooks with Chef{{% /fragment %}}
- {{% fragment %}}Chef Utensils{{% /fragment %}}
- {{% fragment %}}Questions{{% /fragment %}}

{{% note %}}
- About why, _not_ how to use it
- That can be another talk!
{{% /note %}}

{{% /section %}}
