+++
outputs = ["Reveal"]
title = "Quantifying your reliance on Open Source software"
url = "/dmd-ignite/slides/"
[author]
[author.website]
link = "https://www.jvt.me/elsewhere/"
name = "www.jvt.me/elsewhere"
[author.dmd]
link = "https://dmd.tanna.dev"
name = "dmd.tanna.dev"
[reveal_hugo]
auto_slide = 15000
+++
