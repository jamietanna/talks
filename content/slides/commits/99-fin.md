---
---
{{% section %}}

## Thanks for having me!

- Slides - https://talks.jvt.me/commits/
- Me - https://www.jvt.me/
- Blog post - [_89 things I know about Git commits_](https://www.jvt.me/posts/2024/07/12/things-know-commits/)

{{% /section %}}
