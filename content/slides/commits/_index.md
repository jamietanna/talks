+++
outputs = ["Reveal"]
title = "89 things I know about Git commits"
url = "/commits/slides/"
[reveal_hugo]
slide_number = "c/t"
show_slide_number = "speaker"
[author]
[author.website]
link = "https://www.jvt.me/elsewhere/"
name = "www.jvt.me/elsewhere"
+++
