---
weight: 3
---
{{% section %}}
## Pre University

{{% note %}}
The "came for the campus" bit
{{% /note %}}

---

Social group

{{% note %}}
- Only a couple of tech people
- Not really a big group to talk / share
{{% /note %}}

---

<img src="/came-for-the-campus-stayed-for-the-community/img/cat-jamie.jpg" alt="" />

{{% note %}}
First time seeing in the rain - still cool
{{% /note %}}

---

<img src="/came-for-the-campus-stayed-for-the-community/img/jubilee.jpg" alt="" />
{{% /section %}}
