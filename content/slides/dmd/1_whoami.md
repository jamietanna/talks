---
---
{{% section %}}

## `/usr/bin/whoami`

- Jamie Tanna (he/him)
- https://www.jvt.me
- https://www.jvt.me/elsewhere/
- Developer Experience 🚀
- Currently @ Elastic, previously Deliveroo

{{% note %}}
with an interest towards solving engineering-facing problems, aiming to make folks more effective in their roles,
{{% /note %}}

{{% /section %}}

{{% section %}}

### Timeline of events

<ul>
<li class=fragment>
2024-10: This talk!
</li>
<li class=fragment>
2023-07: First public talk
</li>
<li class=fragment>
2023-02: Created the dependency-management-data project
</li>
<li class=fragment>
2022-08: First iteration with Dependabot
</li>
<li class=fragment>
2019: "Formally" considering it
</li>
<li class=fragment>
2017: Hacking around
</li>
</ul>

{{% note %}}
I've been interested in this problem for years, and only recently have I had the tools to invest in it
{{% /note %}}

{{% /section %}}
