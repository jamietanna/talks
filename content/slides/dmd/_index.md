+++
outputs = ["Reveal"]
title = "Quantifying your reliance on Open Source software"
url = "/dmd/slides/"
[author]
[author.dmd]
link = "https://dmd.tanna.dev"
name = "dmd.tanna.dev"
[author.website]
link = "https://www.jvt.me/elsewhere/"
name = "www.jvt.me/elsewhere"
+++
