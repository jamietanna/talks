---
---

{{% section %}}

## How does it work?

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<div class=code>

```sh
# produce some data that DMD can import, i.e.
npx @jamietanna/renovate-graph@latest --token $GITHUB_TOKEN your-org/repo
# set up the database
dmd db init --db dmd.db
# import renovate-graph data
dmd import renovate --db dmd.db 'out/*.json'
# optionally, generate advisories
dmd db generate advisories --db dmd.db
# then you can start querying it
sqlite3 dmd.db 'select count(*) from renovate'
```

</div>

<style>
div.code .highlight {
font-size: 150%;
}
div.code pre.code-wrapper {
width: 100%;
}
</style>

{{% note %}}
From here, the data is then imported and able to be queried.
{{% /note %}}

{{% /section %}}

{{% section %}}

### Datasources

{{% /section %}}

{{% section %}}

Currently:

<ul>
<li class=fragment>

<code>renovate-graph</code>: Use <a href=https://docs.renovatebot.com>Renovate</a> as the source
</li>
<li class=fragment>
<code>dependabot-graph</code>: Use GitHub's Dependabot API
</li>
<li class=fragment>
Software Bill of Materials (SPDX, CycloneDX)
</li>
<li class=fragment>
<code>endoflife-checker</code>: Pull in i.e. AWS infrastructure
</li>
<li class=fragment>
More welcome!
</li>
</ul>


{{% note %}}
Renovate **doesn't** require you to be using it anyway

endoflife-checker needs a rename
{{% /note %}}


{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

When importing:

- Converts to an underlying data model (in SQLite)
- Uses that for internal querying + enrichment

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

Once ingested, write SQL to your heart's content 🤓 i.e.

<ul>
<li class=fragment>
"which repos use a vulnerable version of Log4J"
</li>
<li class=fragment>
"how many repos are using a version of the Datadog SDK that's older than ..."
</li>
<li class=fragment>
"what is our most used direct/transitive dependency?"
</li>
</ul>

{{% /section %}}

{{% section %}}

### Reports

{{% note %}}
A lot of common queries can be replaced with:
{{% /note %}}

{{% /section %}}

{{% section %}}

Pre-baked, Open Source'd queries:

```text
$ dmd report --help

  advisories                 Report advisories that are available for packages or dependencies in use
  dependenton                Report usage of a given dependency
  golangCILint               Query usages of golangci-lint, tracked as a source-based dependency
  infrastructure-advisories  Report infrastructure advisories
  licenses                   Report license information for package dependencies
  mostPopularDockerImages    Query the most popular Docker registries, namespaces and images in use
  mostPopularPackageManagers Query the most popular package managers in use
  policy-violations          Report policy violations that are found for packages or dependencies
```

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

### Advisories

{{% note %}}
{{% /note %}}

{{% /section %}}


{{% section %}}

Right now we can write SQL queries to ask:

<ul>
<li class=fragment>

_what Terraform modules and versions are being used across the org_?


</li>
<li class=fragment>

_which teams are using the Gin web framework_?

</li>
</ul>

{{% note %}}
with the DB access we get out-of-the-box ...
{{% /note %}}

{{% /section %}}

{{% section %}}

But what if we could ask:

<ul>
<li class=fragment>

_are any of our projects relying on libraries that no longer are recommended by our language guilds_?

</li>
<li class=fragment>

_how much time should my team(s) be planning in the next quarter to upgrade their AWS infrastructure_?

</li>
</ul>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<iframe src="https://giphy.com/embed/lrc1TZHRYxj7lGM3Vg" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/NoraFromQueens-awkwafina-is-nora-from-queens-lrc1TZHRYxj7lGM3Vg">via GIPHY</a></p>

{{% note %}}
I need more data
{{% /note %}}

{{% /section %}}

{{% section %}}

Dependency advisory data sources:

- [endoflife.date](https://endoflife.date)
- [deps.dev](https://deps.dev)
- [ecosyste.ms](https://ecosyste.ms)

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

AWS infrastructure advisory data sources:

- [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker/)

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

🤫

{{% note %}}
Don't wanna share internal things publicly

{{% /note %}}

{{% /section %}}

{{% section %}}

Custom advisories 🦸

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

Community provided advisories via [-contrib](https://gitlab.com/tanna.dev/dependency-management-data-contrib):

```sql
INSERT INTO custom_advisories (
  package_pattern,
  package_manager,
  version,
  version_match_strategy,
  advisory_type,
  description
) VALUES (
  'github.com/golang/mock',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'golang/mock is no longer maintained, and active development been moved to github.com/uber/mock'
);
```

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

### Policies

{{% note %}}
Alternatively, for a much more powerful way to write queries
{{% /note %}}

{{% /section %}}

{{% section %}}

<img src=http://www.openpolicyagent.org/img/opa-horizontal-color.png alt="Open Policy Agent logo" />

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

<!-- TODO: update to `rego` once released -->

```go
package policy

import future.keywords.contains
import future.keywords.if

default advisory_type := "UNMAINTAINED"

deny contains "Use the new GitLab.com server" if
  startswith(input.dependency.package_name, "gitlab.example.com/")
```

</div>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

<!-- TODO: update to `rego` once released -->

```go
// Versions of Gin >= 1.9
deny contains msg if {
	input.dependency.package_manager in {"gomod", "golang"}
	input.dependency.package_name = "github.com/gin-gonic/gin"
	versions[0] in {"v1", "1"}
	to_number(versions[1]) >= 9
	msg := sprintf("%s. Versions of Gin since v1.9.0 have shipped " +
      "ByteDance/sonic as an optional dependency, but it still " +
      "appears as a dependency, and could be in use - more " +
      "details in " +
      "https://github.com/gin-gonic/gin/issues/3653", [prefix])
}


```

</div>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

### Ownership

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<ul style="list-style-type: none">
<li>

_Who does this production service using end-of-life software belong to_?

</li>

<li class=fragment>

`dmd owners` + `owners` table to the rescue!

</li>
</ul>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

<div style="font-size: 150%">

```sql
select
  distinct
  sboms.platform,
  sboms.organisation,
  sboms.repo,
  owner
from
  sboms
  left join owners
  on  sboms.platform     = owners.platform
  and sboms.organisation = owners.organisation
  and sboms.repo         = owners.repo
-- where ...
```

</div>

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

### Repo metadata



{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

_How many customer-facing services are running an outdated version of this internal library?_

{{% note %}}
{{% /note %}}

{{% /section %}}


{{% section %}}

And in the `repository_metadata` table:

| repo                | additional_metadata  |
|---------------------|----------------------|
| api-service         | <code style="font-size: 80%">{"customer_facing": "true"}</code> |
| examples            |  |                     |
| business-service    | <code style="font-size: 80%">{"customer_facing": "false"}</code>                     |


{{% /section %}}

{{% section %}}

<style>
div.code .highlight {
font-size: 150%;
}
div.code pre.code-wrapper {
width: 100%;
}
</style>


<div class=code>

```sql
select
  distinct
  sboms.platform,
  sboms.organisation,
  sboms.repo,
  (case json_extract(repository_metadata.additional_metadata,
    '$.customer_facing') when "true" then true
   else false
   end) as is_customer_facing
from
  sboms
  left join repository_metadata
  on  sboms.platform     = repository_metadata.platform
  and sboms.organisation = repository_metadata.organisation
  and sboms.repo         = repository_metadata.repo
-- where ...
```

</div>

{{% /section %}}

{{% section %}}

### Example project

- https://gitlab.com/tanna.dev/dependency-management-data-example
- https://dependency-management-data-example.fly.dev

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

### Contrib project

- https://gitlab.com/tanna.dev/dependency-management-data-contrib

{{% note %}}
{{% /note %}}

{{% /section %}}
