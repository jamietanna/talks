---
---
{{% section %}}

## Reasons to blog

---

### Leave the internet better than you found it

<img src="https://imgs.xkcd.com/comics/wisdom_of_the_ancients.png" alt="xkcd 979: &quot;Wisdom of the Ancients&quot;: A person sitting at a desk, shaking the screen in frustration as they find a forum post with the same error as they had, marked as resolved, but without a solution posted" style="width: 75%;">

---

### Remember and show off all the great stuff you've done

---

### For the dopamine hits of the traffic

![](https://media.jvt.me/7e69b1931b.png)

Via [_Phew, that's a lot of traffic 😅 What happens when a blog post goes viral?_](https://www.jvt.me/posts/2022/10/14/blog-viral/)

---

### Learn by teaching

{{% note %}}
Minimal example, not share work code
{{% /note %}}

---

### Help others

---

### Free labour for your employer

![Gru explains his plan meme - "Solve difficult problems at work", "Spend evenings and weekends blogging about it", then "Your employer gets free labour", which Gru is surprised by](https://media.jvt.me/6a1242dc9f.jpeg)

{{< fragment >}} It's a choice {{< /fragment >}}

{{< fragment >}} <a href="https://www.jvt.me/posts/2022/10/22/tech-industry-free-labour/"><em>Does the tech industry thrive on free work?</em></a> {{< /fragment >}}

{{% note %}}
- make the choice
- carry on with your work problems, but write the solutions publicly, with a minimal example
- you get your time back, have something to follow through with
{{% /note %}}

---

### On your employer's blog

---

### Because you are an awesome, interesting person, and the world will care what you have to say

{{% /section %}}
