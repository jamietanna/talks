---
---
{{% section %}}

I'm here to talk to you about the resurgence of blogging in the recent years, and why you should blog, too

<ul>
<li class=fragment>
<a href="https://matthiasott.com/articles/into-the-personal-website-verse"><em>Into the Personal-Website-Verse</em> - Matthias Ott</a>
</li>
<li class=fragment>
<a href="https://localghost.dev/blog/building-a-website-like-it-s-1999-in-2022/_"><em>Building a website like it's 1999... in 2022</em> - Sophie Koonin</a>
</li>
<li class=fragment>Destruction of Twitter</li>
</ul>

{{% /section %}}
