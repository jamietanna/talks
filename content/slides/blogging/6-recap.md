---
---
{{% section %}}

## Recap

<ul>
<li class=fragment>
It's <em>your</em> blog
</li>
<li class=fragment>
You don't have to write publicly... but it helps
</li>
<li class=fragment>
Own your URLs!
</li>
<li class=fragment>
Don't aim for perfection
</li>
<li class=fragment>
Choose a schedule that works for you - including none
</li>
<li class=fragment>
Learn through teaching
</li>
</ul>

{{% /section %}}
