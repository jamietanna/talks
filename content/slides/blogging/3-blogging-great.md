---
---
{{% section %}}

## Blogging is great

---

⚡

[_Blogumentation: blogging as a form of blogumentation_](https://www.jvt.me/posts/2017/06/25/blogumentation/)

<ul>
<li class=fragment>
Solve problem ✅
</li>
<li class=fragment>
Write blog post ✅
</li>
</ul>

---

- Number of blog posts: 691
- Number of blogumentation posts: 441

---

[_How blogging has affected me, as a neurodiverse person_](https://www.jvt.me/posts/2023/10/04/blogging-neurodiversity/)

<ul>
<li class=fragment>
Write blog post ✍
</li>
<li class=fragment>
Forget about the thing 🗑️
</li>
<li class=fragment>
Did I write a post about <em>that problem</em>? 🤔
</li>
<li class=fragment>
Find the blog post in the future 🧙
</li>
</ul>

---

<style>
kbd {
    border: 4px solid black;
    border-radius: 10px;
    padding: 5px;
    font-family: monospace;
}
</style>

<span style="font-size: 250%">
<kbd>Ctrl</kbd>+<kbd>F</kbd>
</span>

---

Why am I on stage then?

{{< fragment >}} https://www.jvt.me/posts/2023/10/07/why-blog/ {{< /fragment >}}

{{% /section %}}
