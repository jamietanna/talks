---
---
{{% section %}}

## Reasons not to blog

---

### "I don't have anything to say"

{{< fragment >}} [Letters to a New Developer](https://letterstoanewdeveloper.com/) {{< /fragment >}}

{{% note %}}
Learn this week?
Learn at conf?
{{% /note %}}

---

### "There are better people who can blog about this"

{{% note %}}
Diversity
{{% /note %}}

---

### "I'm not very good at writing"

---

### "I don't have the time"

{{% /section %}}
