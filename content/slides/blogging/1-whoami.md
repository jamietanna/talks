---
---
{{% section %}}

👋🏽

<img src="https://www.jvt.me/img/profile.png" alt="Jamie's profile picture" style="width: 30%">

---

I've been blogging since 2016, where my site had ~250 views:

![](https://media.jvt.me/e4cd34a0d9.png)

---

As the years have gone on, I've gained more traffic:

---
![Screenshot of the Matomo "Visits over time" view showing 2017's traffic, which shows a minimum of 2130 views](https://media.jvt.me/e031143e6c.png)

2017 - 2130 views (+755%)

---

![Screenshot of the Matomo "Visits over time" view showing 2018's traffic, which shows a minimum of 10380 views](https://media.jvt.me/3c47d12b1e.png)

2018 - 10380 views (+387%)

---

![Screenshot of the Matomo "Visits over time" view showing 2019's traffic, which shows a minimum of 47270 views](https://media.jvt.me/93381f8a53.png)

2019 - 47270 views (+355%)

---

![Screenshot of the Matomo "Visits over time" view showing 2020's traffic, which shows a minimum of 117545 views](https://media.jvt.me/66d1fa8a0f.png)

2020 - 117545 views (+148%)

---

![Screenshot of the Matomo "Visits over time" view showing 2021's traffic, which shows a minimum of 146772 views](https://media.jvt.me/666ad50d35.png)

2021 - 146772 (+24%)

---

![Screenshot of the Matomo "Visits over time" view showing 2022's traffic, which shows a minimum of 363136 views](https://media.jvt.me/d3965cb2bb.png)

2022 - 363136 views (+147%)

---

2023 - ~322000

---

Have I got off track?

<em class=fragment style="font-family: serif; font-size: 150%">

Sponsored by [DidYouKnowJamieHasAWebsite.co.uk](http://didyouknowjamiehasawebsite.co.uk/)

</em>

{{% note %}}
While we get back on track, let's hear a word from our sponsors
{{% /note %}}

{{% /section %}}
