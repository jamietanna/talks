---
---
{{% section %}}

## Thanks for having me!

- Slides - https://talks.jvt.me/blogging/
- Me - https://www.jvt.me/
- Blog post - [_Why should you blog_](https://www.jvt.me/posts/2023/10/07/why-blog/)
- Tangents cut for time:
  - [_This talk should also be a blog post_](https://www.jvt.me/posts/2023/10/05/talk-also-blog-post/)
  - [_How blogging has affected me, as a neurodiverse person_](https://www.jvt.me/posts/2023/10/04/blogging-neurodiversity/)
- Happy to chat after, come say hey!

{{% /section %}}
