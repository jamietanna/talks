---
---
{{% section %}}

> `oapi-codegen` is a command-line tool and library to convert OpenAPI specifications to Go code, be it [server-side implementations](https://github.com/oapi-codegen/oapi-codegen/#generating-server-side-boilerplate), [API clients](https://github.com/oapi-codegen/oapi-codegen/#generating-api-clients), or simply [HTTP models](https://github.com/oapi-codegen/oapi-codegen/#generating-api-models).

{{% note %}}
Used as:

- it's more idiomatic Go
- it's got wider support than the JVM-based generator that's quite widespread

{{% /note %}}

{{% /section %}}

{{% section %}}

[OpenAPI is Hard (quobix.com)](https://quobix.com/articles/openapi-is-hard/)

> > "How hard can it be?"
>
> It turns out it can be **really hard**. Way harder than I ever expected.

{{% note %}}

- As quobix indicates, OpenAPI is hard - JSON Pointers, different versions of (not quite) JSON Schema
- Led by examples
{{% /note %}}

{{% /section %}}

{{% section %}}

Power in complexity, but complexity has a cost

{{% note %}}
"technically correct" / "most optimal spec" + complexity has a cost

- using the correct, complex OpenAPI description is great, but it will lead to a pain with different tooling support
  - common denominator of usage of versions across projects/tech stacks
- we're led by examples, as we don't know offhand all the different things that users will want to do
  - better documentation / bug reporting needed
{{% /note %}}

{{% /section %}}

{{% section %}}

Fairly widely used<span class=fragment data-fragment-index="1"> (> 10k public usages)</span>

<span class=fragment data-fragment-index="1">

![GitHub's "used by" interface for deepmap/oapi-codegen, showing ~9k repos and ~5k packages using the project](/inherited-oss-project-sooc/github-deepmap-v1.png)
![GitHub's "used by" interface for oapi-codegen/oapi-codegen, showing ~250 repos and ~300 packages using the project](/inherited-oss-project-sooc/github-deepmap-v2.png)
![GitHub's "used by" interface for oapi-codegen/oapi-codegen/v2, showing ~500 repos and ~300 packages using the project](/inherited-oss-project-sooc/github-oapi-codegen-v2.png)

</span>

{{% note %}}
package that isn't necessarily known using (depending on whether tracked in manifest files)
but powers a lot of usage
{{% /note %}}

{{% /section %}}

---

{{< slide transition="none" >}}

![The history of GitHub stars over time for oapi-codegen, showing an upward trend from 2019 to now (2025). There are two red lines on the graph, which indicate minor releases](/inherited-oss-project-sooc/star-history-0.png)

{{% note %}}
- 2019-03: Project released
- ...
- 2022-04: Minor release (+5m)
- 2022-05: Jamie's first contribution
- 2022-05: Minor release
- 2022-07: Jamie starts to get involved in maintenance
{{% /note %}}

---

{{< slide transition="none" >}}

![The history of GitHub stars over time for oapi-codegen, showing an upward trend from 2019 to now (2025). There are two red lines on the graph, which indicate minor releases, and two green lines indicating Jamie's first contributions to the project](/inherited-oss-project-sooc/star-history-1.png)

{{% note %}}
- 2022-04: Minor release (+5m)
- 2022-05: Jamie's first contribution
- 2022-05: Minor release
- 2022-07: Jamie starts to get involved in maintenance
{{% /note %}}

---

{{< slide transition="none" >}}

![The history of GitHub stars over time for oapi-codegen, showing an upward trend from 2019 to now (2025). There are two red lines on the graph, which indicate minor releases, and two green lines indicating Jamie's first contributions to the project. After the green lines, a few more red lines to indicate project maintenance](/inherited-oss-project-sooc/star-history-2.png)


{{% note %}}
- 2022-10: Minor release (+5m)
- 2023-06: Minor release (+8m)
- 2023-08: Minor release (+2m)
- 2023-09: Minor release (+1m)
- 2023-10: Minor release (+1m)
{{% /note %}}


---

{{< slide transition="none" >}}

![The history of GitHub stars over time for oapi-codegen, showing an upward trend from 2019 to now (2025). There are two red lines on the graph, which indicate minor releases, and two green lines indicating Jamie's first contributions to the project. After the green lines, a few more red lines to indicate project maintenance, with a purple line indicating when we started requesting funding from users](/inherited-oss-project-sooc/star-history-3.png)

{{% note %}}
- 2024-01: Minor release (+3m)
- 2024-05: [Call for funding](https://github.com/oapi-codegen/oapi-codegen/discussions/1606)
- 2024-06: Minor release (+5m)
- 2024-06: Got my first abusive email
- 2024-09: Minor release (+3m)
- Now (+4m)

{{% /note %}}

---

{{% section %}}

### What's it like being a maintainer?

{{% /section %}}

{{% section %}}

!["Umm actually" emoji face, to indicate an annoying comment / question from a user](/inherited-oss-project-sooc/um_actually.png)

> "just review my changes"
>
> "it's not that much work to look at a PR"
>
> "it's a straightforward issue"

{{% note %}}
- recently done some work to improve the way we document + test - have to retrofit
- accepting maintenance and ownership forever
  - "no is temporary, yes is forever" - Solomon Hykes
- can't remove without breaking changes (we have a few bits of debt we'd love to clean up)
{{% /note %}}

{{% /section %}}

{{% section %}}

!["Umm actually" emoji face, to indicate an annoying comment / question from a user](/inherited-oss-project-sooc/um_actually.png)

> "what's the ETA?"

{{% note %}}
- overstretched, folks doing more/asking for more than we can keep up
{{% /note %}}

{{% /section %}}


{{% section %}}

!["Umm actually" emoji face, to indicate an annoying comment / question from a user](/inherited-oss-project-sooc/um_actually.png)

> "but you get time to work on the project on work time"

{{% note %}}

- doesn't mean that magically solves things
- I'm very appreciative of it
- given it's heavily used in the Elastic Serverless platform (since before I joined) + 26 total repos

{{% /note %}}

{{% /section %}}

{{% section %}}

!["Umm actually" emoji face, to indicate an annoying comment / question from a user](/inherited-oss-project-sooc/um_actually.png)

> "but you get paid"

{{% note %}}

- "you said the industry thrives on free work"
- and I'm trying to do a bit more only when paid to
- but I also have other projects (blog, DMD), and things I want to do, alongside `oapi-codegen`
- also life things, recover from busy job

{{% /note %}}

{{% /section %}}

{{% section %}}

There's a _lot_ of maintenance

<div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 50%;">
    <img src="/inherited-oss-project-sooc/opensauced-issues-180d.png" alt="The last 180 days of issues raised on the project, showing 78 raised and 37 closed - there are spikes of issues being closed, indicating that someone is only really working on them in batches, rather than regularly working on them" />
  </div>

  <div style="flex: 0 0 50%;">
    <img src="/inherited-oss-project-sooc/opensauced-prs-180d.png" alt="The last 180 days of issues raised on the project, showing 81 raised and 4 merged - there is a spikes of PRs being merged, indicating that someone is only really working on them in batches, rather than regularly working on them" />
  </div>
</div>

<small>Via [OpenSauced insights (last 180 days)](https://app.opensauced.pizza/s/oapi-codegen/oapi-codegen?range=180)</small>

<small>Today:</small>

<p style="margin-top: 0px">

![Current GitHub stats showing 488 open issues and 159 open PRs](/inherited-oss-project-sooc/current-gh-stats.png)

</p>

{{% note %}}
- what's it like maintaining?
- there's so much in the backlog to get on top of!
- trying to also look at very long standing PRs/issues to try and close out / understand the gaps
- **currently** 488 issues + 159 PRs in the main repo
{{% /note %}}

{{% /section %}}

{{% section %}}

### What's it like _for me_ being a maintainer?

{{% /section %}}

{{% section %}}

<video onloadstart="this.playbackRate = 2;" src="https://i.imgur.com/rQIb4Vw.mp4" data-autoplay loop>
</video>

{{% note %}}

- it can be hard to start
- it can be hard to not fix other things while I'm there
- I have ADHD, so can go on "side quests"

{{% /note %}}

{{% /section %}}

{{% section %}}

![An common reaction message for i.e. Slack, where there's an angry looking sock-like thing, which isn't happy they've got a notification above their head](/inherited-oss-project-sooc/pingsock.png)

{{% note %}}
- responding to noisiest users
- not necessarily doing the right thing

Late last year, I received an abusive email, and I was considering to never touch the PR they wanted merged, just out of spite

{{% /note %}}

{{% /section %}}
{{% section %}}

![Milhouse from The Simpsons playing frisbee on his own](https://media1.tenor.com/m/oSf8C3h44bkAAAAd/milhouse-simpsons.gif)

<small>Via tenor.com</small>

{{% note %}}
- busy co-maintainer
- mostly solo
- onboarding new people leads to more work
- less done in short term, more in long term
- governance time and work
  - which is a great segue....
{{% /note %}}

{{% /section %}}

{{% section %}}

<img src="https://pragprog.com/titles/bcosp/program-management-for-open-source-projects/bcosp.jpg" alt="Ben Cotton's book, 'Program Management for Open Source projects'" style="width: 45%"/>

{{% note %}}
But need time
{{% /note %}}

{{% /section %}}
