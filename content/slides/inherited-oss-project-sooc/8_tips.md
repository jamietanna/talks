---
---

{{% section %}}

## How can I be a better Open Source citizen?

{{% /section %}}

{{% section %}}

Be a nice fu🤬cking person!

{{% /section %}}

{{% section %}}

Lead with empathy 🤗

{{% note %}}
- Remember there are people
- ... who have their own lives going on
- i.e. may be trying to squeeze this PR review in just before they go to bed
{{% /note %}}

{{% /section %}}

{{% section %}}

Pay for the privilege 💸

{{% note %}}
- If you're not paying, and you know the OSS project isn't supported by a company, it's peoples' free time
- pay (maintainers, or support through else)
{{% /note %}}

{{% /section %}}

{{% section %}}

Say something nice and/or thank you 💜

{{% note %}}
Most importantly, say something nice

{{% /note %}}

{{% /section %}}
