---
---
{{% section %}}

Open Source is **great**

<span class=fragment>Solving problems and sharing solutions for the good of many</span>

<span class=fragment>Being an Open Source maintainer is rewarding</span>

<span class=fragment>Except for when it isn't 🫣</span>

{{% note %}}
- Unequivocally, Open Source is great
- Fixing problems generally is great
- Being an OSS maintainer is moreso a force multiplier, allowing you to solve so many other problems
- I owe _so much_ to Open Source, and honestly am not sure who I'd be if I'd not have gotten so involved with OSS
{{% /note %}}

{{% /section %}}
