---
---
{{% section %}}

## Why are there "tired maintainer"s out there?

{{% note %}}
Those that missed it, intro slide
{{% /note %}}

{{% /section %}}

{{% section %}}

<p class="fragment fade-out" data-fragment-index="1"><small>Sorry...</small></p>

<div class=fragment data-fragment-index="1" style="margin-top: -100px">

![xkcd 2347: Dependency](https://imgs.xkcd.com/comics/dependency.png)

<small><em>xkcd 2347: Dependency</em></small>

</div>

{{% note %}}
I'm **really sorry** about being the first person to use this one today

I'm not equating myself to this case, but indicating that there are a _lot_ of these projects out there!
{{% /note %}}


{{% /section %}}

{{% section %}}

[Kris Brandow](https://skriptble.me/), via [Fallthrough Episode 3](https://fallthrough.transistor.fm/episodes/do-we-think-you-should-learn-to-code#t=1h5m52s):

> [...] are you ready to potentially have the responsibility of you being a **load bearing member** of society?

<small>(emphasis mine)</small>

{{% note %}}
- "great people use it", "oh god, so many using it"
{{% /note %}}

{{% /section %}}

{{% section %}}

[Does the tech industry thrive on free work? (www.jvt.me)](https://www.jvt.me/posts/2022/10/22/tech-industry-free-labour/)

<span class=fragment>Spoiler: Yes</span>

{{% note %}}
TL;DR:

- Thinking of a solution i.e. when cooking
- Blogging / OSS outside of work as "it's easier"
- But also OSS generally relies on free work by maintainers
{{% /note %}}

{{% /section %}}
