+++
outputs = ["Reveal"]
title = "I inherited this project, and all I got was these angry users"
url = "/inherited-oss-project-sooc/slides/"
custom_css_partial = "custom-partial/inherited-oss-project-sooc.css.html"
custom_end_partial = "custom-partial/inherited-oss-project-sooc.end.html"
[author]
[author.website]
link = "https://www.jvt.me/elsewhere/"
name = "www.jvt.me/elsewhere"
+++
