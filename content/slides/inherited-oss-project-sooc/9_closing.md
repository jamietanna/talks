---
---
{{% section %}}

# Closing

- Remember there are humans behind the issues (users and maintainers)
- Send a thank you to one project, today 💜
- Pay your maintainers!
- Slides: https://talks.jvt.me/inherited-oss-project/

{{% /section %}}

{{% section %}}

## Related blog posts

- https://www.jvt.me/posts/2019/11/06/take-take-take-foss/
- https://www.jvt.me/posts/2024/02/06/dmd-talk-sooc/
- https://www.jvt.me/posts/2022/10/22/tech-industry-free-labour/

{{% /section %}}
