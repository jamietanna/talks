---
---
{{% section %}}

## `/usr/bin/whoami`

- Jamie Tanna (he/him)
- https://www.jvt.me
- https://www.jvt.me/elsewhere/

{{% note %}}
{{% /note %}}

{{% /section %}}

{{% section %}}

## Open Sourcerer 🧙🏼

https://www.jvt.me/open-source/

<span class=fragment>
Best known for:

- [`oapi-codegen`](https://github.com/oapi-codegen/oapi-codegen/)
- Previously: [Jenkins Job DSL plugin](https://github.com/jenkinsci/job-dsl-plugin/) and [Wiremock](https://github.com/wiremock/wiremock)

</span>

{{% note %}}
- Creator of projects
- Solver of my own problems
- ~40 projects and a few services around:
  - IndieWeb
  - Java
  - Dependency Management, SBOMs + insights
  - Go
  - My blog (v big resource, > 1000 posts!)
{{% /note %}}

{{% /section %}}
