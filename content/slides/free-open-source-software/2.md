---
weight: 3
---
{{% section %}}
## So why is it important?

---

![Tracey the farmer and her tractor that she can't legally self-repair](/free-open-source-software/img/farmer-tractor.png)

<https://www.wired.com/story/john-deere-farmers-right-to-repair/>

---

![](/free-open-source-software/img/laptop.png)

<https://www.digitaltrends.com/computing/apple-locking-down-macs-from-repairs/>

---

![](/free-open-source-software/img/laptop-windows.png)

<https://itsfoss.com/linux-better-than-windows/>
{{% /section %}}
