---
weight: 2
---

{{< slide template="hacktoberfest" transition="fade-out" transition-speed="fast" >}}

<img src="https://hacktoberfest.digitalocean.com/assets/logo-hf19-header-8245176fe235ab5d942c7580778a914110fa06a23c3d55bf40e2d061809d8785.svg" alt="Hacktoberfest 2019 Logo" style="width: 100%;margin: auto;background-color: #152347;"/>

<div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 45%;">
    <p style="text-align: right;">Anna </p>
    <p style="text-align: right; color: #ff07a6; font-size: 34px">@anna_hax</p>
  </div>
  <div style="flex: 0 0 10%;">
  <p style=""> & </p>
  <p style="color: #ff07a6; font-size: 34px"> - </p>
  </div>
  <div style="flex: 0 0 45%;">
    <p style="text-align: left;"> Jamie</p>
    <p style="text-align: left; color: #ff07a6; font-size: 34px">@jamietanna</p>
  </div>
</div>