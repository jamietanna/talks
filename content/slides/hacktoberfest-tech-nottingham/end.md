---
weight: 999
---
{{% section %}}

{{< slide template="hacktoberfest" >}}

<img src="https://hacktoberfest.digitalocean.com/assets/logo-hf19-header-8245176fe235ab5d942c7580778a914110fa06a23c3d55bf40e2d061809d8785.svg" alt="Hacktoberfest 2019 Logo" style="width: 30%;margin: auto;background-color: #152347;"/>

1. Register at https://hacktoberfest.digitalocean.com/
1. Go to https://github.com/AnnaDodson/hacktoberfest-website
1. Find an issue, or write a blog post
1. Follow the setup + contributing guidelines in the README.md

<p>#TechNott #Hacktoberfest</p>

<small>Thanks for listening</small>
<br />
<small> License: CC-BY-NC-SA-4.0</small>

{{% /section %}}
