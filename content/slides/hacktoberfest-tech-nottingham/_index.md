+++
outputs = ["Reveal"]
title = "Hacktoberfest - Tech Nottingham 2019-10-14"
url = "/hacktoberfest-tech-nottingham/slides/"
[params.reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
[reveal_hugo.templates.hacktoberfest]
class = "hacktoberfest"
background = "#152347"
[author]
[author.jamie]
link = "https://twitter.com/jamietanna"
name = "@JamieTanna"
[author.anna]
link = "https://twitter.com/anna_hax"
name = "@Anna_Hax"
+++
