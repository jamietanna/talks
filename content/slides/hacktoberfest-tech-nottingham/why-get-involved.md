---
weight: 15
---

{{< slide template="hacktoberfest" >}}

{{% section %}}

# Why Get Involved?

---

# We're all using FOSS

{{% note %}}
- Hand up if you use Open Source Software
- Or stops being maintained?
- What if you don't support the newest version of Chrome anymore
{{% /note %}}

---

# If you're using it, shape its future

{{% note %}}
- Have your say
- Use your voice
{{% /note %}}


{{% /section %}}

