---
weight: 5
---
{{< slide template="hacktoberfest" >}}

{{% section %}}
# Who are we?

---

## Jamie

- Free and Open Source champion
- Serial blogger - https://www.jvt.me
- Big fan of getting more folks into contributing to Free and Open Source Software

---


## Anna

- Organise stuff like this
- Big fan of Free and Open Source Software

{{% /section %}}
