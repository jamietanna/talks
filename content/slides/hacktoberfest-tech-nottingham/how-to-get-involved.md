---
weight: 20
---

{{< slide template="hacktoberfest" >}}

{{% section %}}
# How To Get Involved

---

### How do I do it?

- Sign up at <https://hacktoberfest.digitalocean.com/>
- Go to GitHub.com
- Submit 4 Pull Requests
- 🚨 There is now a review process 🚨

---

## Find a project

- [Nottingham.digital](https://github.com/nottinghamdigital/nottinghamdigital.github.io)
- [DDD East Midlands](https://github.com/DDDEastMidlandsLimited/)
- [PHPMiNDS](https://github.com/phpminds/)
- [We Built This For Hacktoberfest - AnnaDodson/hacktoberfest-website](https://github.com/AnnaDodson/hacktoberfest-website)

---

## Find or raise an issue

(this bit can be scary)

---

## Do Some Reading!

{{% note %}}
README is called the README for a reason

Contributing guide

Check the license
{{% /note %}}

---

## Fork + Pull Request

https://blog.dddeastmidlands.com/2019/10/10/how-to-pr-with-your-post.html is a great resource!

{{% note %}}
Check the guides how
{{% /note %}}

---

## Ask for Help

{{% note %}}
People like to help other people

Ask me or Jamie or anyone!

Don't give up
{{% /note %}}

---

# Beware `AntennaGuest`

Connecting over SSH a problem? You may need one of these for a repo you've already `git clone`d:

```sh
git config url.https://github.com/.insteadOf ssh://git@github.com/
git config url.https://github.com/.insteadOf ssh://git@github.com:
git config url.https://github.com/.insteadOf git@github.com:
```

https://www.jvt.me/posts/2019/03/20/git-rewrite-url-https-ssh/

{{% /section %}}
