---
weight: 10
---

{{< slide template="hacktoberfest" >}}

{{% section %}}
# What is Hacktoberfest?

---

## Celebrate Free and Open Source Software

---

## Encourage Open Source Contributions

{{% note %}}
- first time contributions at all
- first time contributions to a project
- promote the idea that contributing is straightforward and that folks should do it more often
{{% /note %}}

---

## Encourage maintainers to make projects more friendly

{{% note %}}
- Update their getting started guides
- Remember that not everyone knows everything
- Change to meet the people that use your products/tools
{{% /note %}}

---

## Chance to be Awesome!
## (and get a free T-shirt)

{{% /section %}}
