---
title: 89 things I know about Git commits (abridged version)
type: talks
events:
- name: OggCamp 2024
  date: 2024-10-13
  url: https://ogg.camp
  type: lightning talk
  video:
description: |
  Some of the things I've learned over a decade of Git usage, and working on writing good commit messages, distilled into a 5 minute lightning talk.
weight: 10
has_slides: true
---
