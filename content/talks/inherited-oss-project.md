---
title: I inherited this project, and all I got was all these angry users
type: talks
events:
- name: State of Open Con 2025
  date: 2025-02-04
  url: https://stateofopencon.com/
  type: keynote
  video: https://www.youtube.com/watch?v=PK8CMcePn2A
  slides:
    url: /inherited-oss-project-sooc/slides/
    license: CC-BY-NC-SA-4.0
description: |
  When I stepped on board to help maintain one of the most widely used Go libraries in a fairly well used niche, I found that my work was cut out for me.

  You'll hear some first-hand examples of the difficulty of being an Open Source maintainer, and some tips you can follow as a good Open Source citizen, to improve life for the maintainers of projects you use.
weight: 10
has_slides: false
---
Although I'd previously helped maintain more well-maintained projects, moving into this project, with one other overworked maintainer who was largely working in the very little spare time they had, I found that my work was cut out for me.

It didn't help that this was also one of the most widely used Go libraries in a fairly well used niche, and a very regularly discussed and recommended option, but the complexity of the domain led to a lot of difficulty in predicting the way that the library is used, and we (as maintainers) are actively driven by the use-cases from our users.

It's taken a couple of years of work to improve our triage process, documentation and re-testing old issues to see if they had been fixed in the meantime, and we've still got _so much_ more to do.

You'll hear some first-hand examples of the difficulty of being an Open Source maintainer, and some tips you can follow as a good Open Source citizen, to improve life for the maintainers of projects you use.

For seasoned maintainers, this will all be very familiar. For folks who are primarily users of Open Source, you'll learn some behind-the-scenes insights into what it's actually like behind the issues/PRs.
