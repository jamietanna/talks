---
title: Write blog posts, not Open Source
type: talks
events:
description: |
  Looking for a good way to give back to Open Source? Consider blogging about the project! Documenting in your own words, for your own specific usecases, alongside your unique point of view could be better than any first-class documentation you could add, as well as being a great skill to have.
weight: 15
---
As someone who's been blogging for ~8 years, and contributing to Open Source for over a decade, I absolutely believe there is space for both contributions upstream and personal blog posts.

However, as someone who's a strong proponent for the personal website, an avid blogger, and believer in the need for a diverse set of voices in the Open Source world, I'd recommend that you consider writing a blog post as an alternative to (or as well as!) sending that first change request.

As a maintainer of [a number of Open Source projects](https://www.jvt.me/open-source/), I love to read about what people are doing with my software. And as someone curious about new libraries and tools that I can add to my toolbelt, I love reading blog posts that reference ways that they've i.e. used a new tool to validate their OpenAPI specifications against their codebase, or finding a faster way to serialise JSON data in Go.

Blogs are also a great point-in-time view of how to do something, and can be more easily searchable than a project's documentation, as Git forges often block crawler access to non-README pages.

In this talk you'll learn why maintainers should be celebrating users writing blog posts (and hopefully upstreaming some documentation changes) as well as why users should look to write their own documentation. You'll also learn why it can be a great thing as a prospective user of a project, too.
