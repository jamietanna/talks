---
title: Shipping services more quickly with design-first OpenAPI contracts
type: talks
events:
- name: Deliveroo Talks
  date: 2022-08-04
  type: talk
  video: https://www.youtube.com/watch?v=wanV_W2AW_0
description: |
  How using OpenAPI has led to being able to ship a new service more effectively, by removing the need to write scaffolding, and instead focus on the business logic.
weight: 15
---
This talk has also been written [up as a blog post](https://www.jvt.me/posts/2022/06/27/roo-openapi-design-first/), and has more information available [in the event writeup](https://deliveroo.engineering/2023/01/24/roo-talks-care-august.html).
