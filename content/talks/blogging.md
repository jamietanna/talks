---
title: This talk could've been a blog post
type: talks
events:
- name: DDD East Midlands
  date: 2023-10-07
  url: https://dddeastmidlands.com/
  type: lightning talk
  video:
description: |
  In recent years, blogging and the personal website have been getting a resurgence. I see blogging as a critical skill for every engineer to make them more well-rounded and effective in their roles, which can be a force-multiplier for someone's career, regardless of how many people read the posts.

  In this talk, you'll learn some tips and tricks to getting started, as well as how to silence the Imposter Syndrome telling you that "you don't have anything worth writing" or that "there are better people who can blog about that thing", because it's just not true!

  Blogging also doesn't just benefit you, but it can help level up your team, and for companies that actively support and encourage engineers blogging about the work they do, it can be a great indication to prospective candidates of the great work you're doing.
weight: 15
has_slides: true
---
