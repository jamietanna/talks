---
title: Quantifying your reliance on Open Source software
type: talks
events:
- name: DevOpsNotts
  date: 2023-07-25
  url: https://www.meetup.com/devops-notts/events/293929326/
  type: talk
  video:
- name: DevOpsDays London
  date: 2023-09-21
  url: https://devopsdays.org/events/2023-london/welcome/
  type: lightning talk
  video: https://www.youtube.com/watch?v=nTt-TVHGZZk
  slides:
    url: /dmd-ignite/slides/
    license: CC-BY-NC-SA-4.0
- name: TechMids
  date: 2023-10-20
  url: https://conf.techmids.io/
  type: talk
  video:
- name: State of Open Con 24
  date: 2024-02-06
  url: https://stateofopencon.com/schedule2024/
  type: talk
  video: https://www.youtube.com/watch?v=xHWmuNDxisw
  slides:
    url: /dmd-sooc/slides/
    license: CC-BY-NC-SA-4.0
- name: Notts TechFast
  date: 2024-02-21
  url: https://www.meetup.com/notts-techfast/events/299079531/
  type: talk
  video:
- name: Digital Lincoln
  date: 2024-06-25
  url: https://www.meetup.com/digitallincoln/events/301577630/
  type: talk
  video:
- name: DTX London
  date: 2024-10-02
  url: https://www.dtxevents.io/london/
  type: talk
  video: https://www.youtube.com/watch?v=a8v0Zu-mBCQ
  slides:
    url: /dmd-dtx/slides/
    license: CC-BY-NC-SA-4.0
- name: OggCamp 2024
  date: 2024-10-12
  url: https://ogg.camp
  type: talk
  video:
  slides:
    url: /dmd/slides/
    license: CC-BY-NC-SA-4.0
- name: Fusion Meetup
  date: 2024-11-21
  url: https://meetup.thefusionhub.co.uk/events/meetup-2024-11-21
  type: talk
  video: https://www.youtube.com/live/EoGDMbMnRws?feature=shared&t=5764
  slides:
    url: /dmd-fusion/slides/
    license: CC-BY-NC-SA-4.0
description: |
  I've always been interested in the composition of Open Source and internal dependencies that my projects use, and after years of wanting some way to query the data, I went ahead and built it!

  Having access to this data has allowed me and my team to get a view of what our most popular languages or frameworks are, which packages we didn't realise we so heavily rely on, determine how many dependencies are using end-of-life software, or just understand the way that internally-built libraries are used across the organisation.

  This data has given my company's developers, leadership and the security team a better view of the landscape of our ever growing dependencies on Open Source, so we can appropriately upgrade, migrate, and support projects.

  In this talk, you'll learn that it's straightforward to do this yourself with Free and Open Source Software, as well as looking at some examples of the data that you can get out of this tooling for your own purposes.
weight: 15
has_slides: true
---
